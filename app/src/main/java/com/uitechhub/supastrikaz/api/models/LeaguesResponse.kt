package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class GetLeaguesResponse(
    @SerializedName("data")
    val `data`: List<LeagueData>,
    @SerializedName("meta")
    val meta: Meta?
)

data class LeaguesByIdResponse(
    @SerializedName("data")
    val `data`: LeagueData,
    @SerializedName("meta")
    val meta: Meta?
)

data class LeagueData(
    @SerializedName("active")
    val active: Boolean,
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("coverage")
    val coverage: Coverage,
    @SerializedName("current_round_id")
    val currentRoundId: Int?,
    @SerializedName("current_season_id")
    val currentSeasonId: Int,
    @SerializedName("current_stage_id")
    val currentStageId: Int?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_cup")
    val isCup: Boolean,
    @SerializedName("is_friendly")
    val isFriendly: Boolean,
    @SerializedName("legacy_id")
    val legacyId: Int?,
    @SerializedName("live_standings")
    val liveStandings: Boolean,
    @SerializedName("logo_path")
    val logoPath: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("type")
    val type: String
)


