package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.GetLeaguesResponse
import com.uitechhub.supastrikaz.api.models.LeaguesByIdResponse
import nl.komponents.kovenant.Promise

interface LeagueService {

    fun getLeagues(includes: List<String>): Promise<GetLeaguesResponse, Exception>

    fun searchLeagueByName(
        leagueName: String,
        includes: List<String>
    ): Promise<GetLeaguesResponse, Exception>

    fun getLeaguesById(
        leagueId: String,
        includes: List<String>
    ): Promise<LeaguesByIdResponse, Exception>
}

class LeagueServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : LeagueService {
    override fun getLeagues(includes: List<String>): Promise<GetLeaguesResponse, Exception> {
        return strikazAPI.load(requestBuilder.getLeagues(includes))
    }

    override fun searchLeagueByName(
        leagueName: String,
        includes: List<String>
    ): Promise<GetLeaguesResponse, Exception> {
        return strikazAPI.load(requestBuilder.searchLeagueByName(leagueName, includes))
    }

    override fun getLeaguesById(
        leagueId: String,
        includes: List<String>
    ): Promise<LeaguesByIdResponse, Exception> {
        return strikazAPI.load(requestBuilder.getLeagueById(leagueId, includes))
    }

}