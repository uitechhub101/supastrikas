package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.FixtureResponse
import com.uitechhub.supastrikaz.api.models.FixturesResponse
import nl.komponents.kovenant.Promise

interface FixtureService {
    fun getFixturesById(
        fixtureId: String,
        includes: List<String>? = null,
        bookmakers: List<String>? = null,
        markets: List<String>? = null
    ): Promise<FixtureResponse, Exception>

    fun getLastUpdatedFixtures(
        includes: List<String>? = null,
        leagues: List<String>? = null,
        fixtures: List<String>? = null,
        bookmakers: List<String>? = null,
        markets: List<String>? = null
    ): Promise<FixturesResponse, Exception>

    fun getFixturesByDate(
        date: String,
        leagues: List<String>? = null,
        markets: List<String>? = null,
        includes: List<String>? = null,
        bookmakers: List<String>? = null,
        status: List<String>? = null
    ): Promise<FixtureResponse, Exception>

    fun getFixturesByDateRange(
        startDate: String,
        endDate: String,
        leagues: List<String>? = null,
        markets: List<String>? = null,
        includes: List<String>? = null,
        bookmakers: List<String>? = null,
        status: List<String>? = null
    ): Promise<FixtureResponse, Exception>

    fun getFixturesByDateRangeForTeam(
        startDate: String,
        endDate: String,
        teamId: String,
        leagues: List<String>? = null,
        markets: List<String>? = null,
        includes: List<String>? = null,
        bookmakers: List<String>? = null,
        status: List<String>? = null
    ): Promise<FixtureResponse, Exception>
}

class FixtureServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : FixtureService {
    override fun getFixturesById(
        fixtureId: String,
        includes: List<String>?,
        bookmakers: List<String>?,
        markets: List<String>?
    ): Promise<FixtureResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getFixturesById(
                fixtureId,
                includes,
                bookmakers,
                markets
            )
        )
    }

    override fun getLastUpdatedFixtures(
        includes: List<String>?,
        leagues: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?,
        markets: List<String>?
    ): Promise<FixturesResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getLastUpdatedFixtures(
                includes,
                leagues,
                fixtures,
                bookmakers,
                markets
            )
        )
    }

    override fun getFixturesByDate(
        date: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ): Promise<FixtureResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getFixturesByDate(
                date,
                leagues,
                markets,
                includes,
                bookmakers,
                status
            )
        )
    }

    override fun getFixturesByDateRange(
        startDate: String,
        endDate: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ): Promise<FixtureResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getFixturesByDateRange(
                startDate,
                endDate,
                leagues,
                markets,
                includes,
                bookmakers,
                status
            )
        )
    }

    override fun getFixturesByDateRangeForTeam(
        startDate: String,
        endDate: String,
        teamId: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ): Promise<FixtureResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getFixturesByDateRangeForTeam(
                startDate,
                endDate,
                teamId,
                leagues,
                markets,
                includes,
                bookmakers,
                status
            )
        )
    }

}