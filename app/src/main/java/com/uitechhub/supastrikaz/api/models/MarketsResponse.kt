package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class AllMarketsResponse(
    @SerializedName("data")
    val `data`: List<MarketsData>
)

data class MarketByIdResponse(
    @SerializedName("data")
    val data: MarketsData
)

data class MarketsData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)

data class FixturesByMarketIdResponse(
    @SerializedName("data")
    val `data`: List<FixturesByMarketIdData>,
    @SerializedName("meta")
    val meta: Meta
)

data class FixturesByMarketIdData(
    @SerializedName("aggregate_id")
    val aggregateId: Any?,
    @SerializedName("assistants")
    val assistants: Assistants,
    @SerializedName("attendance")
    val attendance: Any?,
    @SerializedName("coaches")
    val coaches: Coaches,
    @SerializedName("colors")
    val colors: Any?,
    @SerializedName("commentaries")
    val commentaries: Boolean,
    @SerializedName("deleted")
    val deleted: Boolean,
    @SerializedName("details")
    val details: Any?,
    @SerializedName("flatOdds")
    val flatOdds: FlatOdds,
    @SerializedName("formations")
    val formations: Formations,
    @SerializedName("group_id")
    val groupId: Any?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_placeholder")
    val isPlaceholder: Boolean,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("leg")
    val leg: String,
    @SerializedName("localteam_id")
    val localteamId: Int,
    @SerializedName("neutral_venue")
    val neutralVenue: Boolean,
    @SerializedName("pitch")
    val pitch: Any?,
    @SerializedName("referee_id")
    val refereeId: Int,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("scores")
    val scores: Scores,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("stage_id")
    val stageId: Int,
    @SerializedName("standings")
    val standings: Standings,
    @SerializedName("time")
    val time: Time,
    @SerializedName("venue_id")
    val venueId: Int,
    @SerializedName("visitorteam_id")
    val visitorteamId: Int,
    @SerializedName("weather_report")
    val weatherReport: Any?,
    @SerializedName("winner_team_id")
    val winnerTeamId: Any?,
    @SerializedName("winning_odds_calculated")
    val winningOddsCalculated: Boolean
)



data class FlatOdds(
    @SerializedName("data")
    val `data`: List<FlatOddsData>
)

data class FlatOddsData(
    @SerializedName("bookmaker_event_id")
    val bookmakerEventId: Any?,
    @SerializedName("bookmaker_id")
    val bookmakerId: Int,
    @SerializedName("market_id")
    val marketId: Int,
    @SerializedName("odds")
    val odds: List<Odd>,
    @SerializedName("suspended")
    val suspended: Boolean
)

data class Odd(
    @SerializedName("american")
    val american: Int,
    @SerializedName("dp3")
    val dp3: String,
    @SerializedName("extra")
    val extra: Any?,
    @SerializedName("factional")
    val factional: Any?,
    @SerializedName("handicap")
    val handicap: Any?,
    @SerializedName("label")
    val label: String,
    @SerializedName("last_update")
    val lastUpdate: LastUpdate,
    @SerializedName("probability")
    val probability: String,
    @SerializedName("stop")
    val stop: Boolean,
    @SerializedName("total")
    val total: Any?,
    @SerializedName("value")
    val value: String,
    @SerializedName("winning")
    val winning: Any?
)

data class LastUpdate(
    @SerializedName("date")
    val date: String,
    @SerializedName("timezone")
    val timezone: String,
    @SerializedName("timezone_type")
    val timezoneType: Int
)