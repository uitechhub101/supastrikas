package com.uitechhub.supastrikaz.api.common

import android.util.Log
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object DevTrustSSLCerts {
    internal const val TAG = "NukeSSLCerts"
    var trustAllCerts: Array<TrustManager> = arrayOf()
    fun trust(): SSLContext? {
        try {
            trustAllCerts = arrayOf(CMATrustManager())
            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())

            return sc
        } catch (e: Exception) {
            Log.e("SSL cert trust() failed", "$e")
        }
        return null
    }

    private class CMATrustManager : X509TrustManager {
        override fun getAcceptedIssuers(): Array<X509Certificate?> {
            return arrayOfNulls(0)
        }

        override fun checkClientTrusted(
            certs: Array<X509Certificate?>?,
            authType: String?
        ) {
            // this override method is not used
        }

        override fun checkServerTrusted(
            certs: Array<X509Certificate?>?,
            authType: String?
        ) {
            // this override method is not used
        }
    }
}