package com.uitechhub.supastrikaz.api.interceptors

import android.util.Log
import com.uitechhub.supastrikaz.BuildConfig
import com.uitechhub.supastrikaz.api.NetworkConstants
import com.uitechhub.supastrikaz.api.RequestHeader
import com.uitechhub.supastrikaz.api.RequestHeader.HeaderTypes.Monks
import com.uitechhub.supastrikaz.api.RequestHeader.HeaderTypes.News
import com.uitechhub.supastrikaz.api.RequestHeader.HeaderTypes.None
import com.uitechhub.supastrikaz.api.addHeaderMap
import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response

abstract class RequestInterceptor : Interceptor {

    abstract fun internetAvailable(): Boolean

    abstract fun getBearerToken(): String

    abstract fun internetConnectionUpdate(active: Boolean)

    private fun getAuthorizationHeader(headers: Headers): MutableMap<String, String> {
        val headerType = RequestHeader.HeaderTypes.fromCustomHeader(
            headers.get(RequestHeader.HeaderTypes.requestRequiresAuth)
        )
        // set auth
        return when (headerType) {
            Monks -> mutableMapOf(NetworkConstants.MONKS_API_TOKEN to BuildConfig.MONKS_API_TOKEN)
            News -> mutableMapOf()
            None -> mutableMapOf()
        }
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        var originalRequest = chain.request()
        val originalUrl = originalRequest.url
        val headers = getAuthorizationHeader(originalRequest.headers)

        if (internetAvailable()) {
            internetConnectionUpdate(true)
            try {
                val builder = originalUrl.newBuilder()
                originalRequest = originalRequest.newBuilder()
                    .removeHeader(RequestHeader.HeaderTypes.requestRequiresAuth)
                    .addHeader(
                        NetworkConstants.HEADER_CONTENT_TYPE_KEY,
                        NetworkConstants.HEADER_CONTENT_TYPE_VALUE
                    )
                    .addHeader(
                        NetworkConstants.HEADER_ACCEPT_KEY,
                        NetworkConstants.HEADER_ACCEPT_VALUE
                    )
                    .addHeaderMap(headers)
                    .url(builder.build())
                    .build()
            } catch (ex: Exception) {
                Log.e(this::class.java.simpleName, "$ex")
            }
        } else {
            internetConnectionUpdate(false)
            originalRequest = originalRequest.newBuilder()
                .build()
        }
        return chain.proceed(originalRequest)
    }
}
