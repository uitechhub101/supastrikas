package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.dao.SessionManagerDAO
import javax.inject.Inject

class AuthenticationServiceImpl @Inject constructor(
    val api: StrikazAPI,
    val requestBuilder: RequestBuilder,
    val sessionManagerDAO: SessionManagerDAO
) : AuthenticationService {


}