package com.uitechhub.supastrikaz.api.models

import com.google.gson.annotations.SerializedName

data class FixturesResponse(
    @SerializedName("data")
    val `data`: List<FixturesResponseData>
)

data class FixtureResponse(
    @SerializedName("data")
    val `data`: FixturesResponseData
)

data class FixturesResponseData(
    @SerializedName("aggregate_id")
    val aggregateId: Any?,
    @SerializedName("assistants")
    val assistants: Assistants,
    @SerializedName("attendance")
    val attendance: Any?,
    @SerializedName("bench")
    val bench: BenchPlayersResponse?,
    @SerializedName("coaches")
    val coaches: Coaches,
    @SerializedName("colors")
    val colors: Colors,
    @SerializedName("commentaries")
    val commentaries: Boolean,
    @SerializedName("deleted")
    val deleted: Boolean,
    @SerializedName("details")
    val details: Any?,
    @SerializedName("events")
    val events: Events?,
    @SerializedName("formations")
    val formations: Formations,
    @SerializedName("group_id")
    val groupId: Any?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_placeholder")
    val isPlaceholder: Boolean,
    @SerializedName("league")
    val league: LeaguesByIdResponse?,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("leg")
    val leg: String,
    @SerializedName("lineup")
    val lineup: LineupPlayersResponse?,
    @SerializedName("localCoach")
    val localCoach: LocalCoachResponse?,
    @SerializedName("localteam_id")
    val localteamId: Int,
    @SerializedName("neutral_venue")
    val neutralVenue: Boolean,
    @SerializedName("pitch")
    val pitch: Any?,
    @SerializedName("referee")
    val referee: RefereeResponse?,
    @SerializedName("referee_id")
    val refereeId: Int,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("scores")
    val scores: Scores,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("stage_id")
    val stageId: Int,
    @SerializedName("standings")
    val standings: Standings,
    @SerializedName("stats")
    val stats: FixtureStatsResponse?,
    @SerializedName("time")
    val time: Time,
    @SerializedName("valuebet")
    val valueBet: ValueBetResponse?,
    @SerializedName("venue_id")
    val venueId: Int,
    @SerializedName("visitorCoach")
    val visitorCoach: VisitorCoachResponse?,
    @SerializedName("visitorteam_id")
    val visitorTeamId: Int,
    @SerializedName("weather_report")
    val weatherReport: WeatherReport?,
    @SerializedName("winner_team_id")
    val winnerTeamId: Int,
    @SerializedName("winning_odds_calculated")
    val winningOddsCalculated: Boolean
)

data class BenchPlayersResponse(
    @SerializedName("data")
    val `data`: List<BenchPlayersResponseData>
)

data class Colors(
    @SerializedName("localteam")
    val localteam: Localteam,
    @SerializedName("visitorteam")
    val visitorteam: Visitorteam
)

data class Events(
    @SerializedName("data")
    val `data`: List<EventsData>
)

data class LineupPlayersResponse(
    @SerializedName("data")
    val `data`: List<LineupPlayersResponseData>
)

data class LocalCoachResponse(
    @SerializedName("data")
    val `data`: LocalCoachResponseData
)

data class RefereeResponse(
    @SerializedName("data")
    val `data`: RefereeResponseData
)


data class FixtureStatsResponse(
    @SerializedName("data")
    val `data`: List<FixtureStatsResponseData>
)

data class ValueBetResponse(
    @SerializedName("data")
    val `data`: ValueBetResponseData
)

data class VisitorCoachResponse(
    @SerializedName("data")
    val `data`: VisitorCoachResponseData
)


data class BenchPlayersResponseData(
    @SerializedName("additional_position")
    val additionalPosition: Any?,
    @SerializedName("captain")
    val captain: Any?,
    @SerializedName("fixture_id")
    val fixtureId: Int,
    @SerializedName("formation_position")
    val formationPosition: Any?,
    @SerializedName("number")
    val number: Int,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("player_name")
    val playerName: String,
    @SerializedName("position")
    val position: String,
    @SerializedName("posx")
    val posx: Any?,
    @SerializedName("posy")
    val posy: Any?,
    @SerializedName("stats")
    val stats: Stats,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("type")
    val type: String
)

data class Localteam(
    @SerializedName("color")
    val color: String,
    @SerializedName("kit_colors")
    val kitColors: String
)

data class Visitorteam(
    @SerializedName("color")
    val color: String,
    @SerializedName("kit_colors")
    val kitColors: String
)

data class EventsData(
    @SerializedName("extra_minute")
    val extraMinute: Any?,
    @SerializedName("fixture_id")
    val fixtureId: Int,
    @SerializedName("id")
    val id: Long,
    @SerializedName("injuried")
    val injuried: Any?,
    @SerializedName("minute")
    val minute: Int,
    @SerializedName("on_pitch")
    val onPitch: Boolean,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("player_name")
    val playerName: String,
    @SerializedName("reason")
    val reason: Any?,
    @SerializedName("related_player_id")
    val relatedPlayerId: Any?,
    @SerializedName("related_player_name")
    val relatedPlayerName: Any?,
    @SerializedName("result")
    val result: Any?,
    @SerializedName("team_id")
    val teamId: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("var_result")
    val varResult: Any?
)

data class LineupPlayersResponseData(
    @SerializedName("additional_position")
    val additionalPosition: Any?,
    @SerializedName("captain")
    val captain: Boolean,
    @SerializedName("fixture_id")
    val fixtureId: Int,
    @SerializedName("formation_position")
    val formationPosition: Int,
    @SerializedName("number")
    val number: Int,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("player_name")
    val playerName: String,
    @SerializedName("position")
    val position: String,
    @SerializedName("posx")
    val posx: Any?,
    @SerializedName("posy")
    val posy: Any?,
    @SerializedName("stats")
    val stats: Stats,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("type")
    val type: String
)


data class LocalCoachResponseData(
    @SerializedName("birthcountry")
    val birthcountry: String,
    @SerializedName("birthdate")
    val birthdate: String,
    @SerializedName("birthplace")
    val birthplace: String,
    @SerializedName("coach_id")
    val coachId: Int,
    @SerializedName("common_name")
    val commonName: String,
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("image_path")
    val imagePath: String,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("nationality")
    val nationality: String,
    @SerializedName("team_id")
    val teamId: Int
)

data class RefereeResponseData(
    @SerializedName("common_name")
    val commonName: String,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("lastname")
    val lastname: String
)

data class FixtureStatsResponseData(
    @SerializedName("attacks")
    val attacks: Attacks,
    @SerializedName("ball_safe")
    val ballSafe: Int,
    @SerializedName("corners")
    val corners: Int,
    @SerializedName("fixture_id")
    val fixtureId: Int,
    @SerializedName("fouls")
    val fouls: Int,
    @SerializedName("free_kick")
    val freeKick: Any?,
    @SerializedName("goal_attempts")
    val goalAttempts: Int,
    @SerializedName("goal_kick")
    val goalKick: Any?,
    @SerializedName("goals")
    val goals: Int,
    @SerializedName("injuries")
    val injuries: Int,
    @SerializedName("offsides")
    val offsides: Int,
    @SerializedName("passes")
    val passes: Passes,
    @SerializedName("penalties")
    val penalties: Int,
    @SerializedName("possessiontime")
    val possessiontime: Int,
    @SerializedName("redcards")
    val redcards: Int,
    @SerializedName("saves")
    val saves: Int,
    @SerializedName("shots")
    val shots: FixtureShotStats,
    @SerializedName("substitutions")
    val substitutions: Int,
    @SerializedName("tackles")
    val tackles: Int,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("throw_in")
    val throwIn: Any?,
    @SerializedName("yellowcards")
    val yellowcards: Int,
    @SerializedName("yellowredcards")
    val yellowredcards: Int
)

data class Attacks(
    @SerializedName("attacks")
    val attacks: Int,
    @SerializedName("dangerous_attacks")
    val dangerousAttacks: Int
)


data class FixtureShotStats(
    @SerializedName("blocked")
    val blocked: Int,
    @SerializedName("insidebox")
    val insidebox: Int,
    @SerializedName("offgoal")
    val offgoal: Int,
    @SerializedName("ongoal")
    val ongoal: Int,
    @SerializedName("outsidebox")
    val outsidebox: Int,
    @SerializedName("total")
    val total: Int
)

data class ValueBetResponseData(
    @SerializedName("fixture_id")
    val fixtureId: Int,
    @SerializedName("predictions")
    val predictions: Predictions
)

data class Predictions(
    @SerializedName("bet")
    val bet: String,
    @SerializedName("bookmaker")
    val bookmaker: String,
    @SerializedName("fair_odd")
    val fairOdd: Double,
    @SerializedName("odd")
    val odd: Double,
    @SerializedName("stake")
    val stake: Double
)

data class VisitorCoachResponseData(
    @SerializedName("birthcountry")
    val birthcountry: String,
    @SerializedName("birthdate")
    val birthdate: String,
    @SerializedName("birthplace")
    val birthplace: Any?,
    @SerializedName("coach_id")
    val coachId: Int,
    @SerializedName("common_name")
    val commonName: String,
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("image_path")
    val imagePath: Any?,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("nationality")
    val nationality: String,
    @SerializedName("team_id")
    val teamId: Int
)

data class Upcoming(
    @SerializedName("data")
    val `data`: List<UpcomingData>
)

data class UpcomingData(
    @SerializedName("aggregate_id")
    val aggregateId: Any?,
    @SerializedName("assistants")
    val assistants: Assistants,
    @SerializedName("attendance")
    val attendance: Any?,
    @SerializedName("coaches")
    val coaches: Coaches,
    @SerializedName("colors")
    val colors: Any?,
    @SerializedName("commentaries")
    val commentaries: Boolean,
    @SerializedName("deleted")
    val deleted: Boolean,
    @SerializedName("details")
    val details: Any?,
    @SerializedName("formations")
    val formations: Formations,
    @SerializedName("group_id")
    val groupId: Any?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_placeholder")
    val isPlaceholder: Boolean,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("leg")
    val leg: String,
    @SerializedName("localteam_id")
    val localteamId: Int,
    @SerializedName("neutral_venue")
    val neutralVenue: Boolean,
    @SerializedName("pitch")
    val pitch: Any?,
    @SerializedName("referee_id")
    val refereeId: Any?,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("scores")
    val scores: Scores,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("stage_id")
    val stageId: Int,
    @SerializedName("standings")
    val standings: Standings,
    @SerializedName("time")
    val time: Time,
    @SerializedName("venue_id")
    val venueId: Int?,
    @SerializedName("visitorteam_id")
    val visitorteamId: Int,
    @SerializedName("weather_report")
    val weatherReport: Any?,
    @SerializedName("winner_team_id")
    val winnerTeamId: Any?,
    @SerializedName("winning_odds_calculated")
    val winningOddsCalculated: Boolean
)

