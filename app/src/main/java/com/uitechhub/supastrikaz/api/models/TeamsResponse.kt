package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class TeamsResponse(
    @SerializedName("data")
    val `data`: List<TeamResponseData>,
    @SerializedName("meta")
    val meta: Meta?
)

data class TeamResponse(
    @SerializedName("data")
    val `data`: TeamResponseData,
    @SerializedName("meta")
    val meta: Meta?
)

data class TeamResponseData(
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("current_season_id")
    val currentSeasonId: Int,
    @SerializedName("founded")
    val founded: Int,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_placeholder")
    val isPlaceholder: Boolean,
    @SerializedName("legacy_id")
    val legacyId: Int,
    @SerializedName("logo_path")
    val logoPath: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("national_team")
    val nationalTeam: Boolean,
    @SerializedName("short_code")
    val shortCode: String,
    @SerializedName("twitter")
    val twitter: String?,
    @SerializedName("venue_id")
    val venueId: Int,
    @SerializedName("upcoming")
    val upcoming: Upcoming?,
    @SerializedName("squad")
    val squad: Squad?
)


data class Squad(
    @SerializedName("data")
    val `data`: List<SquadData>
)

data class SquadData(
    @SerializedName("appearences")
    val appearences: Any?,
    @SerializedName("assists")
    val assists: Any?,
    @SerializedName("blocks")
    val blocks: Any?,
    @SerializedName("captain")
    val captain: Int,
    @SerializedName("cleansheets")
    val cleansheets: Any?,
    @SerializedName("crosses")
    val crosses: Crosses,
    @SerializedName("dispossesed")
    val dispossesed: Any?,
    @SerializedName("dribbles")
    val dribbles: DribblesStats,
    @SerializedName("duels")
    val duels: DuelsStats,
    @SerializedName("fouls")
    val fouls: FoulsStats,
    @SerializedName("goals")
    val goals: GoalsStats?,
    @SerializedName("hit_post")
    val hitPost: Any?,
    @SerializedName("injured")
    val injured: Boolean,
    @SerializedName("inside_box_saves")
    val insideBoxSaves: Any?,
    @SerializedName("interceptions")
    val interceptions: Any?,
    @SerializedName("lineups")
    val lineups: Any?,
    @SerializedName("minutes")
    val minutes: Any?,
    @SerializedName("number")
    val number: Any?,
    @SerializedName("owngoals")
    val owngoals: Any?,
    @SerializedName("passes")
    val passes: Passes,
    @SerializedName("penalties")
    val penalties: Penalties,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("position_id")
    val positionId: Int,
    @SerializedName("rating")
    val rating: Any?,
    @SerializedName("redcards")
    val redcards: Any?,
    @SerializedName("saves")
    val saves: Any?,
    @SerializedName("shots")
    val shots: ShotsStats,
    @SerializedName("substitute_in")
    val substituteIn: Any?,
    @SerializedName("substitute_out")
    val substituteOut: Any?,
    @SerializedName("substitutes_on_bench")
    val substitutesOnBench: Any?,
    @SerializedName("tackles")
    val tackles: Any?,
    @SerializedName("yellowcards")
    val yellowcards: Any?,
    @SerializedName("yellowred")
    val yellowred: Any?
)

data class Crosses(
    @SerializedName("accurate")
    val accurate: Any?,
    @SerializedName("total")
    val total: Any?
)

data class Penalties(
    @SerializedName("committed")
    val committed: Any?,
    @SerializedName("missed")
    val missed: Any?,
    @SerializedName("saves")
    val saves: Any?,
    @SerializedName("scores")
    val scores: Any?,
    @SerializedName("won")
    val won: Any?
)

