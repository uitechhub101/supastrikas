package com.uitechhub.supastrikaz.api

import com.google.gson.Gson
import com.uitechhub.supastrikaz.api.services.ConfigService
import javax.inject.Inject


/**
 * Top level request builder class holding all api requests
 * Each function represents a StrikazRequest contract to be executed.
 */
class RequestBuilder @Inject constructor(
    val gson: Gson
) {
    val config: Any
        get() = ConfigService.strikazConfig


    fun getLeagues(includes: List<String>?): GetLeaguesRequest {
        return GetLeaguesRequest(config, gson, includes)
    }

    fun getLeagueById(leagueId: String, includes: List<String>): GetLeagueByIDRequest {
        return GetLeagueByIDRequest(config, gson, leagueId, includes)
    }

    fun searchLeagueByName(leagueName: String, includes: List<String>): SearchLeagueByNameRequest {
        return SearchLeagueByNameRequest(config, gson, leagueName, includes)
    }


    fun getSeasonById(
        includes: List<String>?,
        seasonId: Int
    ): GetSeasonByIdRequest {
        return GetSeasonByIdRequest(config, gson, includes, seasonId)
    }

    fun searchTeamByName(teamName: String, includes: List<String>?) =
        SearchTeamByNameRequest(config, gson, teamName, includes)

    fun getTeamByIdRequest(
        teamId: String,
        includes: List<String>?,
        seasons: List<String>?,
        teamIds: List<String>?
    ) = GetTeamByIdRequest(config, gson, teamId, includes, seasons, teamIds)

    fun getLiveScoresNow(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ) = GetLiveScoresNow(config, gson, leagues, markets, fixtures, includes, bookmakers)

    fun getAllLiveScores(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ) = GetAllLiveScores(config, gson, leagues, markets, fixtures, includes, bookmakers)

    fun getAllMarketsRequest() = GetAllMarketsRequest(config, gson)

    fun getMarketByIDRequest(
        marketId: String
    ) = GetMarketByIDRequest(config, gson, marketId)

    fun getAllFixturesByMarketIDRequest(
        marketId: String
    ) = GetAllFixturesByMarketIDRequest(config, gson, marketId)

    fun getStandingsBySeasonIdRequest(
        seasonId: String,
        includes: List<String>?,
        stageIds: List<String>?,
        groupIds: List<String>?
    ) = GetStandingsBySeasonIDRequest(config, gson, seasonId, includes, stageIds, groupIds)

    fun getStandingsBySeasonIdAndDate(
        seasonId: String,
        date: String
    ) = GetStandingsBySeasonIdAndDate(config, gson, seasonId, date)

    fun getStandingsCorrectionsBySeasonId(
        seasonId: String
    ) = GetStandingsCorrectionsBySeasonIDRequest(config, gson, seasonId)

    fun getPlayerById(
        playerId: String,
        includes: List<String>?
    ) = GetPlayerById(config, gson, playerId, includes)

    fun searchPlayerByName(playerName: String, includes: List<String>?) =
        SearchPlayerByName(config, gson, playerName, includes)

    fun getPlayersForCountryId(countryId: String, includes: List<String>?) =
        GetPlayersForCountryId(config, gson, countryId, includes)

    fun getFixturesById(
        fixtureId: String,
        includes: List<String>?,
        bookmakers: List<String>?,
        markets: List<String>?
    ) = GetFixturesById(config, gson, fixtureId, includes, bookmakers, markets)

    fun getLastUpdatedFixtures(
        includes: List<String>?,
        leagues: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?,
        markets: List<String>?
    ) = GetLastUpdatedFixtures(config, gson, includes, leagues, fixtures, bookmakers, markets)

    fun getFixturesByDate(
        date: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ) = GetFixturesByDate(config, gson, date, leagues, markets, includes, bookmakers, status)

    fun getFixturesByDateRange(
        startDate: String,
        endDate: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ) = GetFixturesByDateRange(
        config,
        gson,
        startDate,
        endDate,
        leagues,
        markets,
        includes,
        bookmakers,
        status
    )

    fun getFixturesByDateRangeForTeam(
        startDate: String,
        endDate: String,
        teamId: String,
        leagues: List<String>?,
        markets: List<String>?,
        includes: List<String>?,
        bookmakers: List<String>?,
        status: List<String>?
    ) = GetFixturesByDateRangeForTeam(
        config,
        gson,
        startDate,
        endDate,
        teamId,
        leagues,
        markets,
        includes,
        bookmakers,
        status
    )
}