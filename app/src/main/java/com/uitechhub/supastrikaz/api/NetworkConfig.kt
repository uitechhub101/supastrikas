package com.uitechhub.supastrikaz.api

import org.json.JSONObject

class NetworkConfig (configJSON: JSONObject){

    val baseOcapiURL: String = configJSON.getString("baseOcapiURL")
    val remoteConfigUrl: String = configJSON.getString("remoteConfigUrl")
    val ocapiVersion: String = configJSON.getString("ocapiVersion")
    val ocapiClientId: String = configJSON.getString("client_id")

}