package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.CountryPlayerResponse
import com.uitechhub.supastrikaz.api.models.PlayerResponse
import com.uitechhub.supastrikaz.api.models.PlayersResponse
import nl.komponents.kovenant.Promise

interface PlayerService {
    val playerIncludes: List<String>

    fun getPlayerById(
        playerId: String,
        includes: List<String> = playerIncludes
    ): Promise<PlayerResponse, Exception>

    fun searchPlayerByName(
        playerName: String,
        includes: List<String> = playerIncludes
    ): Promise<PlayersResponse, Exception>

    fun getPlayersForCountryId(
        countryId: String,
        includes: List<String> = playerIncludes
    ): Promise<CountryPlayerResponse, Exception>
}


class PlayerServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : PlayerService {
    override val playerIncludes: List<String>
        get() = listOf("team", "stats")

    override fun getPlayerById(
        playerId: String,
        includes: List<String>
    ): Promise<PlayerResponse, Exception> {
        return strikazAPI.load(requestBuilder.getPlayerById(playerId, includes))
    }

    override fun searchPlayerByName(
        playerName: String,
        includes: List<String>
    ): Promise<PlayersResponse, Exception> {
        return strikazAPI.load(requestBuilder.searchPlayerByName(playerName, includes))
    }

    override fun getPlayersForCountryId(
        countryId: String,
        includes: List<String>
    ): Promise<CountryPlayerResponse, Exception> {
        return strikazAPI.load(requestBuilder.getPlayersForCountryId(countryId, includes))
    }

}