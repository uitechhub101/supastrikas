package com.uitechhub.supastrikaz.api

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.http.*

interface API {

    @HEAD
    fun head(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf()
    ): Call<JsonElement>

    @GET
    fun get(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf()
    ): Call<JsonElement>

    @GET
    fun getWithNoResponse(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf()
    ): Call<Unit>

    @POST
    fun post(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @Body body: JsonObject
    ): Call<JsonElement>

    @POST
    @FormUrlEncoded
    fun postFormUrlEncoded(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @FieldMap formParams: Map<String, String>
    ): Call<JsonElement>

    @PUT
    fun put(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @Body body: JsonObject
    ): Call<Unit>

    @PUT
    fun putWithResponse(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @Body body: JsonObject
    ): Call<JsonElement>

    @PUT
    @FormUrlEncoded
    fun putFormUrlEncoded(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @FieldMap formParams: Map<String, String>
    ): Call<JsonElement>

    @PUT
    @FormUrlEncoded
    fun putFormUrlEncodedWithResponse(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @FieldMap formParams: Map<String, String>
    ): Call<JsonElement>

    @PATCH
    fun patch(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @Body body: JsonObject
    ): Call<JsonElement>

    @PATCH
    @FormUrlEncoded
    fun patchFormUrlEncoded(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf(),
        @FieldMap formParams: Map<String, String>
    ): Call<JsonElement>

    @DELETE
    fun delete(
        @Url url: String,
        @HeaderMap headers: Map<String, String> = mapOf()
    ): Call<Unit>

    @DELETE
    fun deleteWithResponse(
        @Url url: String,
        @HeaderMap headers: Map<String, String>
    ): Call<JsonElement>

    companion object {
        fun create(
            retrofitBuilder: Retrofit.Builder,
            baseUrl: String
        ): API = retrofitBuilder
            .baseUrl(baseUrl)
            .build().create(API::class.java)
    }

}