package com.uitechhub.supastrikaz.api

import com.google.gson.Gson
import com.google.gson.JsonElement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import nl.komponents.kovenant.Deferred
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.awaitResponse
import javax.inject.Inject

/**
 * Top-level API layer object - this serves as the entry point for all networking throughout the
 * app; all views/services/etc should use this to fetch data.
 *
 */
class StrikazAPI @Inject constructor(
    private val api: API,
    private val gson: Gson,
    private val saveToken: (token: String) -> Unit
) {
    companion object {
        const val UNKNOWN_ERROR_TEXT =
            "Something went wrong while we tried to retrieve the information you requested"
    }

    private val completableJob = Job()
    private val coroutineScope = CoroutineScope(Dispatchers.IO + completableJob)

    /**
     * Loads an API request and returns the result as a [Promise]
     */
    fun <R> load(request: RequestContract<R>): Promise<R, Exception> {

        val future = deferred<R, Exception>()

        coroutineScope.launch {
            val headers = request.customHeaders + createAuthTypeHeaders(request.authType)

            when (request.httpMethod) {
                HttpMethod.Head -> head(request, headers, future)
                HttpMethod.Get -> get(request, headers, future)
                HttpMethod.GetWithNoResponse -> get(request, headers, future, hasResponse = false)
                HttpMethod.Post -> post(request, headers, future)
                HttpMethod.PostUrlEncodedForm -> post(request, headers, future, urlEncoded = true)
                HttpMethod.Put -> put(request, headers, future)
                HttpMethod.PutUrlEncodedForm -> put(request, headers, future, urlEncoded = true)
                HttpMethod.PutUrlEncodedFormWithResponse -> put(
                    request,
                    headers,
                    future,
                    urlEncoded = true,
                    hasResponse = true
                )
                HttpMethod.PutWithResponse -> put(request, headers, future, hasResponse = true)
                HttpMethod.Patch -> patch(request, headers, future)
                HttpMethod.PatchUrlEncodedForm -> patch(request, headers, future, urlEncoded = true)
                HttpMethod.Delete -> delete(request, headers, future)
                HttpMethod.DeleteWithResponse -> delete(request, headers, future, true)
            }
        }
        return future.promise
    }

    fun <R> batchLoad(requests: List<RequestContract<R>>): Promise<R, Exception> {
        /*
        It's possible to create a "queue" of promises which ensures that operations
        always are executed in order, and that the next promise only starts when the
        previous one is entirely completed. Would need help to figure out clean way for
        handling the success/fail calls

        val queue = PromiseQueue()

        requests.forEach { request ->
            queue.add {
                load(request)
            }
        }
        */

        // This should make the calls in parallel, I am able to verify that we get 201 responses
        // however, this seems to be an issue on the server side for not being being able to handle this
        val promises: List<Promise<R, Exception>> = requests.map { load(it) }

        return promises.last()
    }

    private suspend fun <R> get(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        hasResponse: Boolean = true
    ) {
        try {
            if (hasResponse) {
                val response = api.get(request.url, headers).awaitResponse()
                handleResponse(request, response, future)
            } else {
                val response = api.getWithNoResponse(request.url, headers).awaitResponse()
                handleEmptyResponse(response, future)
            }
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private suspend fun <R> post(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        urlEncoded: Boolean = false
    ) {

        val call: Call<JsonElement> = if (urlEncoded) {
            if (request.bodyParams == null) {
                future.reject(Exception("Please set Map body params for url encoded Post"))
                return
            } else {
                api.postFormUrlEncoded(request.url, headers, request.bodyParams!!)
            }
        } else {
            if (request.jsonBodyParams == null) {
                future.reject(Exception("Please set Json body params for non url encoded Post"))
                return
            } else {
                api.post(request.url, headers, request.jsonBodyParams!!)

            }
        }

        try {
            val response = call.awaitResponse()
            handleResponse(request, response, future)
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private suspend fun <R> put(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        urlEncoded: Boolean = false,
        hasResponse: Boolean = false
    ) {

        if (urlEncoded) {
            urlEncodedPut(request, headers, future, hasResponse)
        } else {
            standardPut(request, headers, future, hasResponse)
        }
    }

    private suspend fun <R> urlEncodedPut(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        hasResponse: Boolean = false
    ) {
        if (request.bodyParams == null) {
            future.reject(Exception("Please set Map body params for url encoded Put") as Exception)
            return
        } else {
            try {
                val response = if (hasResponse) {
                    api.putFormUrlEncodedWithResponse(request.url, headers, request.bodyParams!!)
                        .awaitResponse()
                } else {
                    api.putFormUrlEncoded(request.url, headers, request.bodyParams!!)
                        .awaitResponse()
                }

                handleResponse(request, response, future)
            } catch (e: Exception) {
                handleException(e, future)
            }
        }
    }

    private suspend fun <R> standardPut(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        hasResponse: Boolean = false
    ) {
        if (request.jsonBodyParams == null) {
            future.reject(Exception("Please set Map body params for Put") as Exception)
            return
        }

        try {
            if (hasResponse) {
                val response = api.putWithResponse(request.url, headers, request.jsonBodyParams!!)
                    .awaitResponse()
                handleResponse(request, response, future)
            } else {
                val response =
                    api.put(request.url, headers, request.jsonBodyParams!!).awaitResponse()
                handleEmptyResponse(response, future)
            }
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private suspend fun <R> patch(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        urlEncoded: Boolean = false
    ) {
        val call: Call<JsonElement> = if (urlEncoded) {
            if (request.bodyParams == null) {
                future.reject(Exception("Please set Map body params for url encoded Patch"))
                return
            } else {
                api.patchFormUrlEncoded(request.url, headers, request.bodyParams!!)
            }
        } else if (request.jsonBodyParams == null) {
            future.reject(Exception("Please set Map json body params for patch"))
            return
        } else {
            api.patch(request.url, headers, request.jsonBodyParams!!)
        }

        try {
            val response = call.awaitResponse()
            handleResponse(request, response, future)
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private suspend fun <R> delete(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>,
        hasResponse: Boolean = false
    ) {
        try {
            if (hasResponse) {
                val response = api.deleteWithResponse(request.url, headers).awaitResponse()
                handleResponse(request, response, future)
            } else {
                val response = api.delete(request.url, headers).awaitResponse()
                handleEmptyResponse(response, future)
            }
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private suspend fun <R> head(
        request: RequestContract<R>,
        headers: Map<String, String>,
        future: Deferred<R, Exception>
    ) {

        try {
            val response = api.head(request.url, headers).awaitResponse()
            handleResponse(request, response, future)
        } catch (e: Exception) {
            handleException(e, future)
        }
    }

    private fun <R> handleResponse(
        request: RequestContract<R>,
        response: Response<JsonElement>,
        future: Deferred<R, Exception>
    ) {
        if (response.isSuccessful) {
            extractAndSaveToken(response.headers())
            val body = response.body() ?: throw Exception("Empty body")

            val parsedResponse = request.parse(gson, body)
            future.resolve(parsedResponse)
        } else {
            val exception = response.errorBody().use {
                val jsonString = it?.string() ?: ""
                if (jsonString.isNotEmpty()) {
                    gson.fromJson(jsonString, Exception::class.java)
                } else {
                    Exception(UNKNOWN_ERROR_TEXT)
                }
            }
            throw exception
        }
    }

    private fun <R> handleEmptyResponse(
        response: Response<Unit>,
        future: Deferred<R, Exception>
    ) {
        if (!response.isSuccessful) {
            val exception = response.errorBody().use {
                val jsonString = it?.string() ?: ""
                if (jsonString.isNotEmpty()) {
                    gson.fromJson(jsonString, Exception::class.java)
                } else {
                    Exception(UNKNOWN_ERROR_TEXT)
                }
            }
            throw exception
        }

        future.resolve(Unit as R)
    }

    private fun <R> handleException(e: Exception, future: Deferred<R, Exception>) {
        if (e is Exception) {
            future.reject(e)
        } else {
            val exception: Exception = Exception(e).apply {
//                this.fault = Fault(message = e.message ?: "Unable to complete API request.")
            }
            future.reject(exception)
        }
    }

    /**
     * Creates a header map of custom header values representing the auth type of the request.
     * An OkHttp interceptor will intercept these headers before sending the request and use
     * them to determine the auth type and automatically add the correct authorization headers.
     */
    private fun createAuthTypeHeaders(authType: RequestHeader.HeaderTypes) =
        mapOf(RequestHeader.HeaderTypes.requestRequiresAuth to authType.customHeaderValue)


    private fun extractAndSaveToken(header: okhttp3.Headers) {
        val token = header.get(NetworkConstants.AUTHORIZATION)
        token?.let(saveToken)
    }
}