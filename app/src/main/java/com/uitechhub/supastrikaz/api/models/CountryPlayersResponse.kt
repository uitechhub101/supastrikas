package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class CountryPlayerResponse(
    @SerializedName("data")
    val `data`: List<CountryPlayerResponseData>
)

data class CountryPlayerResponseData(
    @SerializedName("birthcountry")
    val birthcountry: String,
    @SerializedName("birthdate")
    val birthdate: String,
    @SerializedName("birthplace")
    val birthplace: String?,
    @SerializedName("common_name")
    val commonName: String,
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("height")
    val height: String?,
    @SerializedName("image_path")
    val imagePath: String,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("nationality")
    val nationality: String,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("position_id")
    val positionId: Int,
    @SerializedName("team_id")
    val teamId: Int?,
    @SerializedName("weight")
    val weight: String?
)