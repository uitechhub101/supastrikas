package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName

data class PlayersResponse(
    @SerializedName("data")
    val `data`: List<PlayerResponseData>,
    @SerializedName("meta")
    val meta: Meta
)


data class PlayerResponse(
    @SerializedName("data")
    val `data`: PlayerResponseData,
    @SerializedName("meta")
    val meta: Meta
)

data class PlayerResponseData(
    @SerializedName("birthcountry")
    val birthcountry: String,
    @SerializedName("birthdate")
    val birthdate: String,
    @SerializedName("birthplace")
    val birthplace: String,
    @SerializedName("common_name")
    val commonName: String,
    @SerializedName("country_id")
    val countryId: Int,
    @SerializedName("display_name")
    val displayName: String,
    @SerializedName("firstname")
    val firstname: String,
    @SerializedName("fullname")
    val fullname: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("image_path")
    val imagePath: String,
    @SerializedName("lastname")
    val lastname: String,
    @SerializedName("nationality")
    val nationality: String,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("position_id")
    val positionId: Int,
    @SerializedName("stats")
    val stats: PlayerResponseStats?,
    @SerializedName("team")
    val team: TeamResponse?,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("weight")
    val weight: Any?
)

data class PlayerResponseStats(
    @SerializedName("data")
    val `data`: List<PlayerResponseStatsData>
)

data class PlayerResponseStatsData(
    @SerializedName("appearences")
    val appearences: Int,
    @SerializedName("assists")
    val assists: Int,
    @SerializedName("blocks")
    val blocks: Any?,
    @SerializedName("captain")
    val captain: Int,
    @SerializedName("cleansheets")
    val cleansheets: Any?,
    @SerializedName("crosses")
    val crosses: Crosses,
    @SerializedName("dispossesed")
    val dispossesed: Int,
    @SerializedName("dribbles")
    val dribbles: DribblesStats,
    @SerializedName("duels")
    val duels: DuelsStats,
    @SerializedName("fouls")
    val fouls: FoulsStats,
    @SerializedName("goals")
    val goals: Int,
    @SerializedName("hit_post")
    val hitPost: Any?,
    @SerializedName("inside_box_saves")
    val insideBoxSaves: Int,
    @SerializedName("interceptions")
    val interceptions: Int,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("lineups")
    val lineups: Int,
    @SerializedName("minutes")
    val minutes: Int,
    @SerializedName("owngoals")
    val owngoals: Int,
    @SerializedName("passes")
    val passes: Passes,
    @SerializedName("penalties")
    val penalties: Penalties,
    @SerializedName("player_id")
    val playerId: Int,
    @SerializedName("rating")
    val rating: Any?,
    @SerializedName("redcards")
    val redcards: Int,
    @SerializedName("saves")
    val saves: Int,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("shots")
    val shots: ShotsStats,
    @SerializedName("substitute_in")
    val substituteIn: Int,
    @SerializedName("substitute_out")
    val substituteOut: Int,
    @SerializedName("substitutes_on_bench")
    val substitutesOnBench: Int,
    @SerializedName("tackles")
    val tackles: Any?,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("type")
    val type: String,
    @SerializedName("yellowcards")
    val yellowcards: Int,
    @SerializedName("yellowred")
    val yellowred: Int
)
