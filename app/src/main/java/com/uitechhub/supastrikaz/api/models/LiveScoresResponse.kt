package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class LiveScoreResponse(
    @SerializedName("data")
    val `data`: List<LiveScoreData>,
    @SerializedName("meta")
    val meta: Meta
)

data class LiveScoreData(
    @SerializedName("aggregate_id")
    val aggregateId: Any?,
    @SerializedName("assistants")
    val assistants: Assistants,
    @SerializedName("attendance")
    val attendance: Any?,
    @SerializedName("coaches")
    val coaches: Coaches,
    @SerializedName("colors")
    val colors: Any?,
    @SerializedName("commentaries")
    val commentaries: Boolean,
    @SerializedName("deleted")
    val deleted: Boolean,
    @SerializedName("details")
    val details: Any?,
    @SerializedName("formations")
    val formations: Formations,
    @SerializedName("group_id")
    val groupId: Any?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_placeholder")
    val isPlaceholder: Boolean,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("leg")
    val leg: String,
    @SerializedName("localteam_id")
    val localteamId: Int,
    @SerializedName("neutral_venue")
    val neutralVenue: Boolean,
    @SerializedName("pitch")
    val pitch: Any?,
    @SerializedName("referee_id")
    val refereeId: Int,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("scores")
    val scores: Scores,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("stage_id")
    val stageId: Int,
    @SerializedName("standings")
    val standings: Standings,
    @SerializedName("time")
    val time: Time,
    @SerializedName("venue_id")
    val venueId: Int,
    @SerializedName("visitorteam_id")
    val visitorteamId: Int,
    @SerializedName("weather_report")
    val weatherReport: WeatherReport,
    @SerializedName("winner_team_id")
    val winnerTeamId: Any?,
    @SerializedName("winning_odds_calculated")
    val winningOddsCalculated: Boolean
)