package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.AllMarketsResponse
import com.uitechhub.supastrikaz.api.models.FixturesByMarketIdResponse
import com.uitechhub.supastrikaz.api.models.MarketByIdResponse
import nl.komponents.kovenant.Promise

interface MarketsService {

    fun getAllMarkets(): Promise<AllMarketsResponse, Exception>

    fun getMarketsByID(marketId: String): Promise<MarketByIdResponse, Exception>

    fun getAllFixturesByMarketID(
        marketId: String
    ): Promise<FixturesByMarketIdResponse, Exception>
}

class MarketsServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : MarketsService {
    override fun getAllMarkets(): Promise<AllMarketsResponse, Exception> {
        return strikazAPI.load(requestBuilder.getAllMarketsRequest())
    }

    override fun getMarketsByID(marketId: String): Promise<MarketByIdResponse, Exception> {
        return strikazAPI.load(requestBuilder.getMarketByIDRequest(marketId))
    }

    override fun getAllFixturesByMarketID(marketId: String): Promise<FixturesByMarketIdResponse, Exception> {
        return strikazAPI.load(requestBuilder.getAllFixturesByMarketIDRequest(marketId))
    }

}