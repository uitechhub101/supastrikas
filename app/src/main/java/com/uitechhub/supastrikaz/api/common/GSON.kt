

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken

/**
 * Global [Gson] initializer, so tests and app code in multiple modules can all create a [Gson]
 * instance with the same configuration across the app.
 */
object GSON {

    fun create(prettyPrint: Boolean = false, withExposeAnnotation: Boolean = false): Gson {
        val bldr = GsonBuilder()

        if (prettyPrint) {
            bldr.setPrettyPrinting()
        }

        if (withExposeAnnotation) {
            bldr.excludeFieldsWithoutExposeAnnotation()
        }

        return bldr.create()
    }

    fun <T> toJson(gson: Gson?, objectValue: T, withExposeAnnotation: Boolean = false): JsonObject {
        return if (gson != null) {
            JsonParser.parseString(gson.toJson(objectValue)).asJsonObject!!
        } else {
            JsonParser.parseString(
                create(true, withExposeAnnotation).toJson(objectValue)
            ).asJsonObject
        }
    }

    fun toJson(str: String) = JsonParser.parseString(str).asJsonObject

    fun toJson(map: Map<String, String>): JsonObject {
        val jsonObject = JsonObject()
        map.forEach {
            jsonObject.addProperty(it.key, it.value)
        }
        return jsonObject
    }

    /**
     * Serializes a JSON String, representing an array of JSON objects, into a generic [List]
     */
    fun <T> toList(json: String): List<T> {
        return create().fromJson<List<T>>(json, object : TypeToken<List<T>>() {}.type)
    }

    /**
     * Serializes object into json string
     */
    fun <T> toJsonString(obj: T): String = create().toJson(obj)


    /**
     * De-serializes string into object
     */
    fun <T> fromJson(json: String, type: Class<T>): T = create().fromJson(json, type)


}