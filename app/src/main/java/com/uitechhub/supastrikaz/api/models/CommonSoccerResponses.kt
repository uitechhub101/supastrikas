package com.uitechhub.supastrikaz.api.models

import com.google.gson.annotations.SerializedName

data class Assistants(
    @SerializedName("first_assistant_id")
    val firstAssistantId: Any?,
    @SerializedName("fourth_official_id")
    val fourthOfficialId: Any?,
    @SerializedName("second_assistant_id")
    val secondAssistantId: Any?
)

data class Coverage(
    @SerializedName("predictions")
    val predictions: Boolean,
    @SerializedName("topscorer_assists")
    val topscorerAssists: Boolean,
    @SerializedName("topscorer_cards")
    val topscorerCards: Boolean,
    @SerializedName("topscorer_goals")
    val topscorerGoals: Boolean
)

data class Coaches(
    @SerializedName("localteam_coach_id")
    val localteamCoachId: Int,
    @SerializedName("visitorteam_coach_id")
    val visitorteamCoachId: Int
)

data class Formations(
    @SerializedName("localteam_formation")
    val localteamFormation: String,
    @SerializedName("visitorteam_formation")
    val visitorteamFormation: String
)

data class Scores(
    @SerializedName("et_score")
    val etScore: String?,
    @SerializedName("ft_score")
    val ftScore: String?,
    @SerializedName("ht_score")
    val htScore: String?,
    @SerializedName("localteam_pen_score")
    val localteamPenScore: Int?,
    @SerializedName("localteam_score")
    val localteamScore: Int,
    @SerializedName("ps_score")
    val psScore: Any?,
    @SerializedName("visitorteam_pen_score")
    val visitorteamPenScore: Int?,
    @SerializedName("visitorteam_score")
    val visitorteamScore: Int
)

data class Standings(
    @SerializedName("localteam_position")
    val localteamPosition: Int,
    @SerializedName("visitorteam_position")
    val visitorteamPosition: Int
)

data class Time(
    @SerializedName("added_time")
    val addedTime: Int?,
    @SerializedName("extra_minute")
    val extraMinute: Int?,
    @SerializedName("injury_time")
    val injuryTime: Any?,
    @SerializedName("minute")
    val minute: Int?,
    @SerializedName("second")
    val second: Int?,
    @SerializedName("starting_at")
    val startingAt: StartingAt,
    @SerializedName("status")
    val status: String
)

data class WeatherReport(
    @SerializedName("clouds")
    val clouds: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("coordinates")
    val coordinates: Coordinates,
    @SerializedName("humidity")
    val humidity: String,
    @SerializedName("icon")
    val icon: String,
    @SerializedName("pressure")
    val pressure: Int,
    @SerializedName("temperature")
    val temperature: Temperature,
    @SerializedName("temperature_celcius")
    val temperatureCelcius: TemperatureCelcius,
    @SerializedName("type")
    val type: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("wind")
    val wind: Wind
)

data class StartingAt(
    @SerializedName("date")
    val date: String,
    @SerializedName("date_time")
    val dateTime: String,
    @SerializedName("time")
    val time: String,
    @SerializedName("timestamp")
    val timestamp: Int,
    @SerializedName("timezone")
    val timezone: String
)

data class Coordinates(
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lon")
    val lon: Double
)

data class Temperature(
    @SerializedName("temp")
    val temp: Double,
    @SerializedName("unit")
    val unit: String
)

data class TemperatureCelcius(
    @SerializedName("temp")
    val temp: Double,
    @SerializedName("unit")
    val unit: String
)

data class Wind(
    @SerializedName("degree")
    val degree: Int,
    @SerializedName("speed")
    val speed: String
)

data class Passes(
    @SerializedName("accurate")
    val accurate: Int,
    @SerializedName("percentage")
    val percentage: Double,
    @SerializedName("accuracy")
    val accuracy: Any?,
    @SerializedName("key_passes")
    val keyPasses: Int?,
    @SerializedName("total")
    val total: Int
)

data class Stats(
    @SerializedName("cards")
    val cards: CardsStats,
    @SerializedName("dribbles")
    val dribbles: DribblesStats,
    @SerializedName("duels")
    val duels: DuelsStats,
    @SerializedName("fouls")
    val fouls: FoulsStats,
    @SerializedName("goals")
    val goals: GoalsStats,
    @SerializedName("other")
    val other: OtherStats,
    @SerializedName("passing")
    val passing: PassingStats,
    @SerializedName("rating")
    val rating: Any?,
    @SerializedName("shots")
    val shots: ShotsStats
)

data class ShotsStats(
    @SerializedName("shots_total")
    val shotsTotal: Int?,
    @SerializedName("shots_on_goal")
    val shotsOnGoal: Int?,
    @SerializedName("shots_on_target")
    val shotsOnTarget: Int?,
    @SerializedName("shots_off_target")
    val shotsOffTarget: Int?,
)

data class DribblesStats(
    @SerializedName("attempts")
    val attempts: Int,
    @SerializedName("dribbled_past")
    val dribbledPast: Int,
    @SerializedName("success")
    val success: Int
)

data class DuelsStats(
    @SerializedName("total")
    val total: Int,
    @SerializedName("won")
    val won: Int
)

data class FoulsStats(
    @SerializedName("committed")
    val committed: Int,
    @SerializedName("drawn")
    val drawn: Int
)

data class GoalsStats(
    @SerializedName("assists")
    val assists: Int,
    @SerializedName("conceded")
    val conceded: Int,
    @SerializedName("owngoals")
    val owngoals: Int,
    @SerializedName("scored")
    val scored: Int
)

data class PassingStats(
    @SerializedName("accurate_passes")
    val accuratePasses: Int,
    @SerializedName("crosses_accuracy")
    val crossesAccuracy: Int,
    @SerializedName("key_passes")
    val keyPasses: Int,
    @SerializedName("passes")
    val passes: Int,
    @SerializedName("passes_accuracy")
    val passesAccuracy: Int,
    @SerializedName("total_crosses")
    val totalCrosses: Int
)

data class CardsStats(
    @SerializedName("redcards")
    val redcards: Int,
    @SerializedName("yellowcards")
    val yellowcards: Int,
    @SerializedName("yellowredcards")
    val yellowredcards: Int
)

data class OtherStats(
    @SerializedName("aerials_won")
    val aerialsWon: Int?,
    @SerializedName("blocks")
    val blocks: Int,
    @SerializedName("clearances")
    val clearances: Int,
    @SerializedName("dispossesed")
    val dispossesed: Int,
    @SerializedName("hit_woodwork")
    val hitWoodwork: Int,
    @SerializedName("inside_box_saves")
    val insideBoxSaves: Int,
    @SerializedName("interceptions")
    val interceptions: Int,
    @SerializedName("minutes_played")
    val minutesPlayed: Int,
    @SerializedName("offsides")
    val offsides: Int,
    @SerializedName("pen_committed")
    val penCommitted: Int,
    @SerializedName("pen_missed")
    val penMissed: Int,
    @SerializedName("pen_saved")
    val penSaved: Int,
    @SerializedName("pen_scored")
    val penScored: Int,
    @SerializedName("pen_won")
    val penWon: Int,
    @SerializedName("punches")
    val punches: Int?,
    @SerializedName("saves")
    val saves: Int,
    @SerializedName("tackles")
    val tackles: Int
)