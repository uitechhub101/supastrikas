package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.LiveScoreResponse
import nl.komponents.kovenant.Promise

interface LiveScoresService {
    fun getAllLiveScores(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ): Promise<LiveScoreResponse, Exception>

    fun getLiveScoresNow(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ): Promise<LiveScoreResponse, Exception>
}

class LiveScoresServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : LiveScoresService {

    override fun getAllLiveScores(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ): Promise<LiveScoreResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getAllLiveScores(
                includes, leagues, markets, fixtures, bookmakers
            )
        )
    }

    override fun getLiveScoresNow(
        includes: List<String>?,
        leagues: List<String>?,
        markets: List<String>?,
        fixtures: List<String>?,
        bookmakers: List<String>?
    ): Promise<LiveScoreResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getLiveScoresNow(
                includes, leagues, markets, fixtures, bookmakers
            )
        )
    }

}