package com.uitechhub.supastrikaz.api.services

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.google.gson.Gson
import nl.komponents.kovenant.Promise

interface ConfigService {

    val configFetchComplete: LiveData<Boolean>

    fun fetchConfig(): Promise<Any, Exception>

    companion object {
        var strikazConfig: Any = Any()
        fun initLocalConfig(gson: Gson, context: Context) {
//            if (!Companion::strikazConfig.isInitialized) {
//                try {
//                    val local = LocalConfigProvider.getLocalConfig(context)
//                    val localConfig =
//                        gson.fromJson(local.toString(), LocalConfig::class.java)
//                    strikazConfig = StrikazConfig(context.resources).mapFromLocalConfig(localConfig)
//                    Log.i("local config retrieved", "strikazConfig")
//                } catch (ex: Exception) {
//                    Log.e("getLocalConfig failed", "$ex")
//                }
//            }
        }
    }
}