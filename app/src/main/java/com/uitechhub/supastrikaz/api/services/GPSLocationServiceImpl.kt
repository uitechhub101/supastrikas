package com.uitechhub.supastrikaz.api.services

import android.Manifest
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.uitechhub.supastrikaz.api.models.LocationLiveData
import com.uitechhub.supastrikaz.api.models.LocationModel
import com.uitechhub.supastrikaz.extfuncs.SingleLiveEvent
import pub.devrel.easypermissions.EasyPermissions
import javax.inject.Inject

class GPSLocationServiceImpl @Inject constructor(
    val context: Context
): GPSLocationService {

    private var location: LocationModel? = null

    private val locationLiveData by lazy {
        LocationLiveData(context)
    }

    private val _locationSource = SingleLiveEvent<LocationModel>()

    /**
     * This will be true if permissions have been granted and location services is on and a location
     * is returned from location updates.
     */
    val _locationEnabled = MediatorLiveData<Boolean>()

    override val locationEnabled: LiveData<Boolean>
        get() = _locationEnabled

    override val locationServicesEnabled: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>(EasyPermissions.hasPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION))
    }

    override val locationSource: SingleLiveEvent<LocationModel>
        get() = _locationSource

    private val observer = Observer<LocationModel> {
        it?.let {
            Log.i("${this.javaClass.simpleName}:", "Location Update: $it")
            location = it
            if (locationSource.hasActiveObservers()) {
                Log.i("${this.javaClass.simpleName}:", "active location observer emit updated")
                _locationSource.postValue(it)
            }
        }
    }
    override fun startLocationUpdate() {
        locationLiveData.observeForever(observer)
    }

    override fun stopLocationUpdates() {
        if (locationLiveData.hasActiveObservers()) {
            locationLiveData.removeObserver(observer)
        }
    }

}