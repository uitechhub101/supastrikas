package com.uitechhub.supastrikaz.api

import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

inline fun <reified T> Call<T>.enqueue(crossinline result: (Result<T>) -> Unit) {
    enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            if (response.isSuccessful) {
                result(Result.Success(call, response))
            } else {
                //todo - maybe send a custom error object|state instead of a Throwable?
                result(Result.Failure(call, Throwable(response.errorBody().toString())))
            }
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            result(Result.Failure(call, t))
        }
    })
}

fun ConnectivityManager.isNetworkAvailable(): Boolean {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
         this.activeNetwork?.let {
            val activeNetwork = this.getNetworkCapabilities(it) ?: return false
            when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } ?: run { false }
    } else {
        val netInfo = this.activeNetworkInfo ?: return false
        netInfo.isConnected
    }
}

/**
 * Adds the map of header values to the receiver, calling [Request.Builder.addHeader] for each one.
 */
fun Request.Builder.addHeaderMap(map: Map<String, String>): Request.Builder {
    for ((name, value) in map) {
        this.addHeader(name, value)
    }

    return this
}