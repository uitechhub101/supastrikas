package com.uitechhub.supastrikaz.api


import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.uitechhub.supastrikaz.BuildConfig
import com.uitechhub.supastrikaz.api.models.*
import com.uitechhub.supastrikaz.extfuncs.appendQueryParameters
import com.uitechhub.supastrikaz.extfuncs.replaceAll

sealed class StrikazRequest<out T>(
    val config: Any,
    val gson: Gson
) : RequestContract<T> {

    companion object {
        // todo - define path param placeholders here
        const val INCLUDES = "include"
        const val SEASONS = "seasons"
        const val TEAMS = "teams"
        const val TEAM_ID = "TEAM_ID"
        const val DATE = "DATE"
        const val TEAM_NAME = "TEAM_NAME"
        const val SEASON_ID = "SEASON_ID"
        const val LEAGUE_NAME = "LEAGUE_NAME"
        const val LEAGUE_ID = "LEAGUE_ID"
        const val API_TOKEN = "api_token"
        const val BOOK_MAKERS = "bookmakers"
        const val LEAGUES = "leagues"
        const val FIXTURES = "fixtures"
        const val STATUS = "status"
        const val MARKETS = "markets"
        const val COUNTRY_ID = "COUNTRY_ID"
        const val PLAYER_ID = "PLAYER_ID"
        const val PLAYER_NAME = "PLAYER_NAME"
        const val START_DATE = "START_DATE"
        const val END_DATE = "END_DATE"
        const val FIXTURE_ID = "FIXTURE_ID"
        const val MARKET_ID = "MARKET_ID"
        const val GROUP_IDS = "GROUP_IDS"
        const val STAGE_IDS = "STAGE_IDS"
        const val DATE_TIME = "DATE_TIME"
    }

    override val url: String
        get() = when (this) {
            is GetLeaguesRequest -> "https://soccer.sportmonks.com/api/v2.0/leagues"
            is GetLeagueByIDRequest -> "https://soccer.sportmonks.com/api/v2.0/leagues/$LEAGUE_ID"
            is GetSeasonByIdRequest -> "https://soccer.sportmonks.com/api/v2.0/seasons/$SEASON_ID"
            is SearchLeagueByNameRequest -> "https://soccer.sportmonks.com/api/v2.0/leagues/search/$LEAGUE_NAME"
            is SearchTeamByNameRequest -> "https://soccer.sportmonks.com/api/v2.0/teams/search/$TEAM_NAME"
            is GetTeamByIdRequest -> "https://soccer.sportmonks.com/api/v2.0/teams/$TEAM_ID"
            is GetAllLiveScores -> "https://soccer.sportmonks.com/api/v2.0/livescores"
            is GetLiveScoresNow -> "https://soccer.sportmonks.com/api/v2.0/livescores/now"
            is GetAllMarketsRequest -> "https://soccer.sportmonks.com/api/v2.0/markets"
            is GetMarketByIDRequest -> "https://soccer.sportmonks.com/api/v2.0/markets/$MARKET_ID"
            is GetAllFixturesByMarketIDRequest -> "https://soccer.sportmonks.com/api/v2.0/markets/$MARKET_ID/fixtures"
            is GetStandingsBySeasonIDRequest -> "https://soccer.sportmonks.com/api/v2.0/standings/season/$SEASON_ID"
            is GetStandingsBySeasonIdAndDate -> "https://soccer.sportmonks.com/api/v2.0/standings/season/$SEASON_ID/date/$DATE_TIME"
            is GetStandingsCorrectionsBySeasonIDRequest -> "https://soccer.sportmonks.com/api/v2.0/corrections/season/$SEASON_ID"
            is GetPlayerById -> "https://soccer.sportmonks.com/api/v2.0/players/$PLAYER_ID?include=stats"
            is GetPlayersForCountryId -> "https://soccer.sportmonks.com/api/v2.0/countries/$COUNTRY_ID/players"
            is SearchPlayerByName -> "https://soccer.sportmonks.com/api/v2.0/players/search/$PLAYER_NAME"
            is GetFixturesByDate -> "https://soccer.sportmonks.com/api/v2.0/fixtures/date/$DATE"
            is GetFixturesByDateRange -> "https://soccer.sportmonks.com/api/v2.0/fixtures/between/$START_DATE/$END_DATE"
            is GetFixturesByDateRangeForTeam -> "https://soccer.sportmonks.com/api/v2.0/fixtures/between/$START_DATE/$END_DATE/$TEAM_ID"
            is GetFixturesById -> "https://soccer.sportmonks.com/api/v2.0/fixtures/$FIXTURE_ID"
            is GetLastUpdatedFixtures -> "https://soccer.sportmonks.com/api/v2.0/fixtures/updates"
        }.replaceAll(pathParameters).appendQueryParameters(queryParams)

    override val pathParameters: Map<String, String>
        get() = when (this) {
            is GetSeasonByIdRequest -> mapOf(
                SEASON_ID to seasonId.toString()
            )
            is SearchLeagueByNameRequest -> mapOf(
                LEAGUE_NAME to leagueName
            )
            is SearchTeamByNameRequest -> mapOf(
                TEAM_NAME to teamName
            )
            is GetTeamByIdRequest -> mapOf(
                TEAM_ID to teamId
            )
            is GetLeagueByIDRequest -> mapOf(
                LEAGUE_ID to leagueId
            )
            is GetMarketByIDRequest -> mapOf(
                MARKET_ID to marketId
            )
            is GetAllFixturesByMarketIDRequest -> mapOf(
                MARKET_ID to marketId
            )
            is GetStandingsBySeasonIDRequest -> mapOf(
                SEASON_ID to seasonId
            )
            is GetStandingsBySeasonIdAndDate -> mapOf(
                SEASON_ID to seasonId,
                DATE_TIME to date
            )
            is GetStandingsCorrectionsBySeasonIDRequest -> mapOf(
                SEASON_ID to seasonId
            )
            is GetPlayerById -> mapOf(
                PLAYER_ID to playerId
            )
            is GetPlayersForCountryId -> mapOf(
                COUNTRY_ID to countryId
            )
            is SearchPlayerByName -> mapOf(
                PLAYER_NAME to playerName
            )
            is GetFixturesByDate -> mapOf(
                DATE to date
            )
            is GetFixturesByDateRange -> mapOf(
                START_DATE to startDate,
                END_DATE to endDate
            )
            is GetFixturesByDateRangeForTeam -> mapOf(
                START_DATE to startDate,
                END_DATE to endDate,
                TEAM_ID to teamId
            )
            is GetFixturesById -> mapOf(
                FIXTURE_ID to fixtureId
            )
            else -> mapOf()
        }

    override val jsonBodyParams: JsonObject?
        get() = when (this) {
            else -> null
        }

    override val bodyParams: Map<String, String>?
        get() = null

    override val customHeaders: Map<String, String>
        get() = when (this) {
            else -> mapOf()
        }

    override val queryParams: Map<String, String>
        get() = when (this) {
            is GetLeaguesRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is GetLeagueByIDRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is GetSeasonByIdRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is SearchLeagueByNameRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is SearchTeamByNameRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is GetTeamByIdRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                seasons?.let {
                    map.put(SEASONS, it.joinToString(","))
                }
                teamIds?.let {
                    map.put(TEAM_ID, it.joinToString(","))
                }
                map
            }
            is GetAllLiveScores -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!fixtures.isNullOrEmpty()) {
                    map.put(FIXTURES, fixtures.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetLiveScoresNow -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!fixtures.isNullOrEmpty()) {
                    map.put(FIXTURES, fixtures.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetAllMarketsRequest, is GetMarketByIDRequest, is GetAllFixturesByMarketIDRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                map
            }
            is GetStandingsBySeasonIDRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map[INCLUDES] = includes.joinToString(",")
                }
                if (!groupIds.isNullOrEmpty()) {
                    map[GROUP_IDS] = groupIds.joinToString(",")
                }
                if (!stageIds.isNullOrEmpty()) {
                    map[STAGE_IDS] = stageIds.joinToString(",")
                }
                map
            }
            is GetStandingsBySeasonIdAndDate,
            is GetStandingsCorrectionsBySeasonIDRequest -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                map
            }
            is GetPlayerById  -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is GetPlayersForCountryId -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            is SearchPlayerByName -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                map
            }
            // requests without query params
            is GetFixturesByDate -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!status.isNullOrEmpty()) {
                    map.put(FIXTURES, status.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetFixturesByDateRange -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!status.isNullOrEmpty()) {
                    map.put(STATUS, status.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetFixturesByDateRangeForTeam -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!status.isNullOrEmpty()) {
                    map.put(FIXTURES, status.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetFixturesById -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
            is GetLastUpdatedFixtures -> {
                val map = mutableMapOf<String, String>()
                map.put(API_TOKEN, BuildConfig.MONKS_API_TOKEN)
                if (!includes.isNullOrEmpty()) {
                    map.put(INCLUDES, includes.joinToString(","))
                }
                if (!leagues.isNullOrEmpty()) {
                    map.put(LEAGUES, leagues.joinToString(","))
                }
                if (!fixtures.isNullOrEmpty()) {
                    map.put(FIXTURES, fixtures.joinToString(","))
                }
                if (!markets.isNullOrEmpty()) {
                    map.put(MARKETS, markets.joinToString(","))
                }
                if (!bookmakers.isNullOrEmpty()) {
                    map.put(BOOK_MAKERS, bookmakers.joinToString(","))
                }
                map
            }
        }

    override val httpMethod: HttpMethod
        get() = when (this) {
            is GetLiveScoresNow,
            is GetAllLiveScores,
            is GetLeaguesRequest,
            is GetTeamByIdRequest,
            is GetLeagueByIDRequest,
            is GetSeasonByIdRequest,
            is GetAllMarketsRequest,
            is GetMarketByIDRequest,
            is SearchTeamByNameRequest,
            is SearchLeagueByNameRequest,
            is GetStandingsBySeasonIDRequest,
            is GetStandingsBySeasonIdAndDate,
            is GetAllFixturesByMarketIDRequest,
            is GetPlayerById,
            is GetPlayersForCountryId,
            is SearchPlayerByName,
            is GetFixturesById,
            is GetFixturesByDate,
            is GetLastUpdatedFixtures,
            is GetFixturesByDateRange,
            is GetFixturesByDateRangeForTeam,
            is GetStandingsCorrectionsBySeasonIDRequest -> HttpMethod.Get

        }

    override val authType: RequestHeader.HeaderTypes
        get() = when (this) {
            is GetPlayerById,
            is GetPlayersForCountryId,
            is SearchPlayerByName,
            is GetLeaguesRequest -> RequestHeader.HeaderTypes.Monks
            is GetLeagueByIDRequest -> RequestHeader.HeaderTypes.Monks
            is GetTeamByIdRequest -> RequestHeader.HeaderTypes.Monks
            is GetSeasonByIdRequest -> RequestHeader.HeaderTypes.Monks
            is SearchTeamByNameRequest -> RequestHeader.HeaderTypes.Monks
            is SearchLeagueByNameRequest -> RequestHeader.HeaderTypes.Monks
            else -> RequestHeader.HeaderTypes.None
        }
}

// TODO - Add API Request Contracts here.

//region Fixtures
/**
 *Fixture by Id
 * Last Updated Fixtures
 * Fixtures By Date
 * Fixtures By Date Range
 * Fixtures By Date Range For Team
 * Fixtures By Multiple ID's
 */


//endregion
//region - LiveScores

class GetAllLiveScores(
    config: Any,
    gson: Gson,
    val leagues: List<String>?,
    val markets: List<String>?,
    val fixtures: List<String>?,
    val includes: List<String>?,
    val bookmakers: List<String>?
) : StrikazRequest<LiveScoreResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): LiveScoreResponse {
        return gson.fromJson(jsonObject, LiveScoreResponse::class.java)
    }
}

class GetLiveScoresNow(
    config: Any,
    gson: Gson,
    val leagues: List<String>?,
    val markets: List<String>?,
    val fixtures: List<String>?,
    val includes: List<String>?,
    val bookmakers: List<String>?
) : StrikazRequest<LiveScoreResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): LiveScoreResponse {
        return gson.fromJson(jsonObject, LiveScoreResponse::class.java)
    }
}

// endregion

//region - Leagues
class GetLeaguesRequest(
    config: Any,
    gson: Gson,
    val includes: List<String>?
) : StrikazRequest<GetLeaguesResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): GetLeaguesResponse {
        return gson.fromJson(jsonObject, GetLeaguesResponse::class.java)
    }
}

class SearchLeagueByNameRequest(
    config: Any,
    gson: Gson,
    val leagueName: String,
    val includes: List<String>?
) : StrikazRequest<GetLeaguesResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): GetLeaguesResponse {
        return gson.fromJson(jsonObject, GetLeaguesResponse::class.java)
    }
}

class GetLeagueByIDRequest(
    config: Any,
    gson: Gson,
    val leagueId: String,
    val includes: List<String>?
) : StrikazRequest<LeaguesByIdResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): LeaguesByIdResponse {
        return gson.fromJson(jsonObject, LeaguesByIdResponse::class.java)
    }
}

//endregion


//region - Seasons

class GetSeasonByIdRequest(
    config: Any,
    gson: Gson,
    val includes: List<String>?,
    val seasonId: Int
) : StrikazRequest<GetSeasonByIdResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): GetSeasonByIdResponse {
        return gson.fromJson(jsonObject, GetSeasonByIdResponse::class.java)
    }
}


//endregion

//region - Teams
class SearchTeamByNameRequest(
    config: Any,
    gson: Gson,
    val teamName: String,
    val includes: List<String>?
) : StrikazRequest<TeamsResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): TeamsResponse {
        return gson.fromJson(jsonObject, TeamsResponse::class.java)
    }
}

class GetTeamByIdRequest(
    config: Any,
    gson: Gson,
    val teamId: String,
    val includes: List<String>?,
    val seasons: List<String>?,
    val teamIds: List<String>?
) : StrikazRequest<TeamResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): TeamResponse {
        return gson.fromJson(jsonObject, TeamResponse::class.java)
    }
}

//endregion

//region Markets

class GetAllMarketsRequest(
    config: Any,
    gson: Gson
) : StrikazRequest<AllMarketsResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): AllMarketsResponse {
        return gson.fromJson(jsonObject, AllMarketsResponse::class.java)
    }
}

class GetMarketByIDRequest(
    config: Any,
    gson: Gson,
    val marketId: String
) : StrikazRequest<MarketByIdResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): MarketByIdResponse {
        return gson.fromJson(jsonObject, MarketByIdResponse::class.java)
    }
}

class GetAllFixturesByMarketIDRequest(
    config: Any,
    gson: Gson,
    val marketId: String
) : StrikazRequest<FixturesByMarketIdResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixturesByMarketIdResponse {
        return gson.fromJson(jsonObject, FixturesByMarketIdResponse::class.java)
    }
}

//endregion

//region Standings

class GetStandingsBySeasonIDRequest(
    config: Any,
    gson: Gson,
    val seasonId: String,
    val includes: List<String>?,
    val stageIds: List<String>?,
    val groupIds: List<String>?
) : StrikazRequest<StandingsResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): StandingsResponse {
        return gson.fromJson(jsonObject, StandingsResponse::class.java)
    }
}

// todo - ensure the response model is accurate.
class GetStandingsBySeasonIdAndDate(
    config: Any,
    gson: Gson,
    val seasonId: String,
    val date: String
) : StrikazRequest<StandingsResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): StandingsResponse {
        return gson.fromJson(jsonObject, StandingsResponse::class.java)
    }
}

class GetStandingsCorrectionsBySeasonIDRequest(
    config: Any,
    gson: Gson,
    val seasonId: String
) : StrikazRequest<StandingsResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): StandingsResponse {
        return gson.fromJson(jsonObject, StandingsResponse::class.java)
    }
}

//endregion

//region Player
class GetPlayerById(
    config: Any,
    gson: Gson,
    val playerId: String,
    val includes: List<String>?
) : StrikazRequest<PlayerResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): PlayerResponse {
        return gson.fromJson(jsonObject, PlayerResponse::class.java)
    }
}

class SearchPlayerByName(
    config: Any,
    gson: Gson,
    val playerName: String,
    val includes: List<String>?
) : StrikazRequest<PlayersResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): PlayersResponse {
        return gson.fromJson(jsonObject, PlayersResponse::class.java)
    }
}

class GetPlayersForCountryId(
    config: Any,
    gson: Gson,
    val countryId: String,
    val includes: List<String>?
) : StrikazRequest<CountryPlayerResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): CountryPlayerResponse {
        return gson.fromJson(jsonObject, CountryPlayerResponse::class.java)
    }
}
//endregion

//region Fixtures
class GetFixturesById(
    config: Any,
    gson: Gson,
    val fixtureId: String,
    val includes: List<String>?,
    val bookmakers: List<String>?,
    val markets: List<String>?
) : StrikazRequest<FixtureResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixtureResponse {
        return gson.fromJson(jsonObject, FixtureResponse::class.java)
    }
}

class GetLastUpdatedFixtures(
    config: Any,
    gson: Gson,
    val includes: List<String>?,
    val leagues: List<String>?,
    val fixtures: List<String>?,
    val bookmakers: List<String>?,
    val markets: List<String>?
) : StrikazRequest<FixturesResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixturesResponse {
        return gson.fromJson(jsonObject, FixturesResponse::class.java)
    }
}

class GetFixturesByDate(
    config: Any,
    gson: Gson,
    val date: String,
    val leagues: List<String>?,
    val markets: List<String>?,
    val includes: List<String>?,
    val bookmakers: List<String>?,
    val status: List<String>?
) : StrikazRequest<FixtureResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixtureResponse {
        return gson.fromJson(jsonObject, FixtureResponse::class.java)
    }
}

class GetFixturesByDateRange(
    config: Any,
    gson: Gson,
    val startDate: String,
    val endDate: String,
    val leagues: List<String>?,
    val markets: List<String>?,
    val includes: List<String>?,
    val bookmakers: List<String>?,
    val status: List<String>?
) : StrikazRequest<FixtureResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixtureResponse {
        return gson.fromJson(jsonObject, FixtureResponse::class.java)
    }
}

class GetFixturesByDateRangeForTeam(
    config: Any,
    gson: Gson,
    val startDate: String,
    val endDate: String,
    val teamId: String,
    val leagues: List<String>?,
    val markets: List<String>?,
    val includes: List<String>?,
    val bookmakers: List<String>?,
    val status: List<String>?
) : StrikazRequest<FixtureResponse>(config, gson) {
    override fun parse(gson: Gson, jsonObject: JsonElement): FixtureResponse {
        return gson.fromJson(jsonObject, FixtureResponse::class.java)
    }
}
//end region