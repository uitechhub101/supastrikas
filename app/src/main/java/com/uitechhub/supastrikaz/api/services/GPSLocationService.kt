package com.uitechhub.supastrikaz.api.services

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.uitechhub.supastrikaz.api.models.LocationModel
import com.uitechhub.supastrikaz.extfuncs.SingleLiveEvent

interface GPSLocationService {

    val locationSource: SingleLiveEvent<LocationModel>

    val locationEnabled: LiveData<Boolean>

    val locationServicesEnabled: MutableLiveData<Boolean>

    fun startLocationUpdate()

    fun stopLocationUpdates()

}