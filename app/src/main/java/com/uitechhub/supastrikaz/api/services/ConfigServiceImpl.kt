package com.uitechhub.supastrikaz.api.services

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.uitechhub.supastrikaz.api.API
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.deferred
import javax.inject.Inject

class ConfigServiceImpl @Inject constructor(
    val gson: Gson,
    val api: API
) : ConfigService {

    private var _configFetchComplete = MutableLiveData<Boolean>()

    override val configFetchComplete: LiveData<Boolean>
        get() = _configFetchComplete

    // todo - maybe avoid fetching the config every time? might be wasteful??
    override fun fetchConfig(): Promise<Any, Exception> = getConfig()

    private fun getConfig(): Promise<Any, Exception> {
        val deferred = deferred<Any, Exception>()
//        try {
//            api.get(Any.remoteConfigUrl).enqueue {
//                when (it) {
//                    is Result.Success -> {
//                        val remoteConfig = gson.fromJson(
//                            it.response.body(),
//                            RemoteConfig::class.java
//                        )
//                        Any = Any.mapFromRemoteConfig(remoteConfig)
//                        deferred.resolve(Any)
//                    }
//
//                    is Result.Failure -> {
//                        //todo - log this failure to perf monitor maybe?
//                        Log.e("config retrieval failed", "${it.error}")
//                        deferred.resolve(Any)
//                    }
//                }
//                _configFetchComplete.postValue(true)
//            }
//        } catch (ex: Exception) {
//            //todo - log this failure to perf monitor maybe?
//            Log.e("getConfig error ", "$ex")
//            deferred.resolve(Any)
//        }
        return deferred.promise
    }
}