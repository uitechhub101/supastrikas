package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.GetSeasonByIdResponse
import com.uitechhub.supastrikaz.api.models.StandingsResponse
import nl.komponents.kovenant.Promise

interface SeasonService {
    fun getSeasonById(
        seasonId: Int,
        includes: List<String>
    ): Promise<GetSeasonByIdResponse, Exception>

    fun getStandings(
        seasonId: String,
        date: String
    ): Promise<StandingsResponse, Exception>

    fun getStandings(
        seasonId: String,
        includes: List<String>?,
        stageIds: List<String>?,
        groupIds: List<String>?
    ): Promise<StandingsResponse, Exception>

    fun getStandingsCorrections(
        seasonId: String
    ): Promise<StandingsResponse, Exception>
}

class SeasonServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : SeasonService {
    override fun getSeasonById(
        seasonId: Int,
        includes: List<String>
    ): Promise<GetSeasonByIdResponse, Exception> {
        return strikazAPI.load(requestBuilder.getSeasonById(includes, seasonId))
    }

    override fun getStandings(
        seasonId: String,
        date: String
    ): Promise<StandingsResponse, Exception> {
        return strikazAPI.load(requestBuilder.getStandingsBySeasonIdAndDate(seasonId, date))
    }

    override fun getStandings(
        seasonId: String,
        includes: List<String>?,
        stageIds: List<String>?,
        groupIds: List<String>?
    ): Promise<StandingsResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getStandingsBySeasonIdRequest(
                seasonId, includes, stageIds, groupIds
            )
        )
    }

    override fun getStandingsCorrections(seasonId: String): Promise<StandingsResponse, Exception> {
        return strikazAPI.load(requestBuilder.getStandingsCorrectionsBySeasonId(seasonId))
    }
}