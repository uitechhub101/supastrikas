package com.uitechhub.supastrikaz.api.services

import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.models.TeamResponse
import com.uitechhub.supastrikaz.api.models.TeamsResponse
import nl.komponents.kovenant.Promise

interface TeamService {

    fun searchTeamByName(
        teamName: String,
        includes: List<String>
    ): Promise<TeamsResponse, Exception>

    fun getTeamById(
        teamId: String,
        includes: List<String>?,
        seasons: List<String>?,
        teamIds: List<String>?
    ): Promise<TeamResponse, Exception>
}

class TeamServiceImpl(
    private val strikazAPI: StrikazAPI,
    private val requestBuilder: RequestBuilder
) : TeamService {
    override fun searchTeamByName(
        teamName: String,
        includes: List<String>
    ): Promise<TeamsResponse, Exception> {
        return strikazAPI.load(requestBuilder.searchTeamByName(teamName, includes))
    }

    override fun getTeamById(
        teamId: String,
        includes: List<String>?,
        seasons: List<String>?,
        teamIds: List<String>?
    ): Promise<TeamResponse, Exception> {
        return strikazAPI.load(
            requestBuilder.getTeamByIdRequest(
                teamId,
                includes,
                seasons,
                teamIds
            )
        )
    }
}