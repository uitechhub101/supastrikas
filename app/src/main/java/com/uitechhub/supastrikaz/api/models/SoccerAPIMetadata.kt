package com.uitechhub.supastrikaz.api.models

import com.google.gson.annotations.SerializedName


data class Meta(
    @SerializedName("pagination")
    val pagination: Pagination,
    @SerializedName("plans")
    val plans: List<Plan>,
    @SerializedName("sports")
    val sports: List<Sport>
)

data class Pagination(
    @SerializedName("count")
    val count: Int,
    @SerializedName("current_page")
    val currentPage: Int,
    @SerializedName("links")
    val links: Links,
    @SerializedName("per_page")
    val perPage: Int,
    @SerializedName("total")
    val total: Int,
    @SerializedName("total_pages")
    val totalPages: Int
)

data class Plan(
    @SerializedName("features")
    val features: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("request_limit")
    val requestLimit: String,
    @SerializedName("sport")
    val sport: String
)

data class Sport(
    @SerializedName("current")
    val current: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)

class Links(
)