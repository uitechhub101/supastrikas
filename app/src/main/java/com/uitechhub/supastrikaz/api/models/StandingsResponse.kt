package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class StandingsResponse(
    @SerializedName("data")
    val `data`: List<StandingsResponseData>,
    @SerializedName("meta")
    val meta: Meta
)

data class StandingsResponseData(
    @SerializedName("id")
    val id: Int,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("resource")
    val resource: String,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("round_name")
    val roundName: Int,
    @SerializedName("season_id")
    val seasonId: Int,
    @SerializedName("stage_id")
    val stageId: Int,
    @SerializedName("stage_name")
    val stageName: String,
    @SerializedName("standings")
    val standings: StandingsDataResponse,
    @SerializedName("type")
    val type: String
)


data class StandingsDataResponse(
    @SerializedName("data")
    val `data`: List<StandingsData>
)

data class StandingsData(
    @SerializedName("away")
    val away: Away,
    @SerializedName("group_id")
    val groupId: Any?,
    @SerializedName("group_name")
    val groupName: Any?,
    @SerializedName("home")
    val home: Home,
    @SerializedName("overall")
    val overall: Overall,
    @SerializedName("points")
    val points: Int,
    @SerializedName("position")
    val position: Int,
    @SerializedName("recent_form")
    val recentForm: String,
    @SerializedName("result")
    val result: Any?,
    @SerializedName("round_id")
    val roundId: Int,
    @SerializedName("round_name")
    val roundName: Int,
    @SerializedName("status")
    val status: String,
    @SerializedName("team_id")
    val teamId: Int,
    @SerializedName("team_name")
    val teamName: String,
    @SerializedName("total")
    val total: Total
)

data class Away(
    @SerializedName("draw")
    val draw: Int,
    @SerializedName("games_played")
    val gamesPlayed: Int,
    @SerializedName("goals_against")
    val goalsAgainst: Int,
    @SerializedName("goals_scored")
    val goalsScored: Int,
    @SerializedName("lost")
    val lost: Int,
    @SerializedName("points")
    val points: Int,
    @SerializedName("won")
    val won: Int
)

data class Home(
    @SerializedName("draw")
    val draw: Int,
    @SerializedName("games_played")
    val gamesPlayed: Int,
    @SerializedName("goals_against")
    val goalsAgainst: Int,
    @SerializedName("goals_scored")
    val goalsScored: Int,
    @SerializedName("lost")
    val lost: Int,
    @SerializedName("points")
    val points: Int,
    @SerializedName("won")
    val won: Int
)

data class Overall(
    @SerializedName("draw")
    val draw: Int,
    @SerializedName("games_played")
    val gamesPlayed: Int,
    @SerializedName("goals_against")
    val goalsAgainst: Int,
    @SerializedName("goals_scored")
    val goalsScored: Int,
    @SerializedName("lost")
    val lost: Int,
    @SerializedName("points")
    val points: Int,
    @SerializedName("won")
    val won: Int
)

data class Total(
    @SerializedName("goal_difference")
    val goalDifference: String,
    @SerializedName("points")
    val points: Int
)