package com.uitechhub.supastrikaz.api

object RequestHeader {

    enum class HeaderTypes {
        Monks,
        News,
        None;

        /**
         * The value assigned to the [requestRequiresAuth], the receiver enum's [name] property.
         */
        val customHeaderValue = this.name

        companion object {

            /**
             * An internal-only header, representing one of the auth type's above, that is detected
             * and removed before sending a request. This is used to tell the HTTP client to
             * automatically add the proper Authorization headers to a request, given the authType
             * is set properly with one of these enum values.
             */
            const val requestRequiresAuth = "RequestRequiresAuth"


            /**
             * Maps the [customHeaderValue] back to an enum type, returning [None] if the name
             * don't match
             */
            fun fromCustomHeader(customHeaderValue: String?) =
                HeaderTypes.values().find { it.name == customHeaderValue } ?: None
        }

    }
}