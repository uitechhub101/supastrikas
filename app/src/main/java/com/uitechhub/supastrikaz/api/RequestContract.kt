package com.uitechhub.supastrikaz.api

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject


interface RequestContract<out T> {
    /**
     * Represents a fully-qualified URL
     */
    val url: String


    /**
     * Key-Value pairs of body parameters to be included in the request.
     * Used as body for regular post calls
     * Override [formUrlEncoded] to URL encode the body (if necessary)
     */
    val jsonBodyParams: JsonObject?

    /**
     * Key-Value pairs to replace the path parameter placeholders within a given url.
     *
     * Example: For a given path `/path/to/endpoint/[PARAM1]/somethingElse/[PARAM2]`,
     *          a map `mapOf("[PARAM1]" to "value1", "[PARAM2]" to "value2")` will
     *          eventually yield the path `/path/to/endpoint/value1/somethingElse/value2`
     *
     * **Note**: The square brackets are required.
     */
    val pathParameters: Map<String, String>


    /**
     * Key-Value pairs of body parameters to be included in the request.
     * Used only with a [formUrlEncoded] call.
     * Override [formUrlEncoded] to URL encode the body (if necessary)
     */
    val bodyParams: Map<String, String>?

    /**
     * The type of authentication, which dictates which `Authorization` headers will be included
     * with the request.
     */
    val authType: RequestHeader.HeaderTypes

    /**
     * Any additional custom headers e added to the request.
     */
    val customHeaders: Map<String, String>

    /**
     * Key-Value pairs to add into the path as arguments
     * `?[key1]=[value1]&[key2]=[value2]`
     * Example: For a given path with includes
     * `?include="country"
     *
     */
    val queryParams: Map<String, String>

    val httpMethod: HttpMethod

    /**
     * Parses the given [JsonObject] into an instance of the response model.
     */
    fun parse(gson: Gson, jsonObject: JsonElement): T
}

enum class HttpMethod {
    Head,
    Get,
    GetWithNoResponse,
    Put,
    Post,
    Patch,
    Delete,
    PutWithResponse,
    PutUrlEncodedFormWithResponse,
    DeleteWithResponse,
    PostUrlEncodedForm,
    PutUrlEncodedForm,
    PatchUrlEncodedForm
}