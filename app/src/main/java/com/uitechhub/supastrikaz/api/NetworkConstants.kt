package com.uitechhub.supastrikaz.api

object NetworkConstants {

    // values
    const val MAX_NETWORK_CACHE_DEFAULT = 60 * 60 * 24
    const val MAX_NETWORK_CACHE_SIZE: Long = 10 * 1024 * 1024
    const val CLIENT_TIME_OUT = 30L

    // headers
    const val MONKS_API_TOKEN = "api_token"
    const val AUTHORIZATION = "Authorization"
    const val AUTHORIZATION_PREFIX_BEARER = "Bearer "
    const val HEADER_CONTENT_TYPE_KEY = "Content-Type"
    const val HEADER_CONTENT_TYPE_VALUE = "application/json"
    const val HEADER_ACCEPT_KEY = "Accept"
    const val HEADER_ACCEPT_VALUE = "application/json"
    const val HEADER_CACHE_CONTROL_KEY = "Cache-Control"
    const val HEADER_CACHE_CONTROL_VALUE = "public, only-if-cached, max-stale= $MAX_NETWORK_CACHE_DEFAULT"
    const val API_RETRY_COUNT = "Retry-Count"
    const val HEADER_CLIENT_ID = "client_id"
}