package com.uitechhub.supastrikaz.api.models
import com.google.gson.annotations.SerializedName


data class GetSeasonByIdResponse(
    @SerializedName("data")
    val `data`: GetSeasonByIdResponseData,
    @SerializedName("meta")
    val meta: Meta
)

data class GetSeasonByIdResponseData(
    @SerializedName("current_round_id")
    val currentRoundId: Any?,
    @SerializedName("current_stage_id")
    val currentStageId: Any?,
    @SerializedName("id")
    val id: Int,
    @SerializedName("is_current_season")
    val isCurrentSeason: Boolean,
    @SerializedName("league_id")
    val leagueId: Int,
    @SerializedName("name")
    val name: String
)
