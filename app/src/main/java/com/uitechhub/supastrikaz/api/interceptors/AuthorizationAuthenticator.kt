package com.uitechhub.supastrikaz.api.interceptors

import android.util.Log
import com.uitechhub.supastrikaz.api.NetworkConstants
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import javax.inject.Inject

class AuthorizationAuthenticator @Inject constructor(
    private val token: () -> String,
    private val refreshToken: () -> Unit,
    private val isLoggedIn: () -> Boolean,
    private val isNetworkAvailable: () -> Boolean
) : Authenticator {

    private fun retryCount(response: Response?): Int =
        response?.request?.header(NetworkConstants.API_RETRY_COUNT)?.toInt() ?: 0

    override fun authenticate(route: Route?, response: Response): Request? {
        return if (isLoggedIn.invoke()) {
            return when (hasAuthorizationBearerToken(response)) {
                false -> {
                    null
                }
                true -> {
                    val previousRetries = retryCount(response)
                    performTokenRefresh(
                        response.request,
                        previousRetries + 1
                    )
                }
            }
        } else {
            null
        }
    }

    private fun hasAuthorizationBearerToken(response: Response?): Boolean {
        return response?.let { res ->
            val authHeader = res.request.header(NetworkConstants.AUTHORIZATION)
            return authHeader?.startsWith(NetworkConstants.AUTHORIZATION_PREFIX_BEARER) ?: false
        } ?: run { false }
    }

    /**
     * Performs token refresh using session manager.
     *
     * Synchronized method to prevent multiple concurrent failures from attempting to use the same
     * refresh token.
     *
     * @param staleRequest
     * @param retryCount
     * @return the previous request that was executed before token refresh or null
     */
    @Synchronized
    private fun performTokenRefresh(staleRequest: Request?, retryCount: Int): Request? {
        if (retryCount > 1 || !isNetworkAvailable.invoke()) {
            return null
        }

        try {
            refreshToken.invoke()
            token.invoke().let { token ->
                return if (token.isNotEmpty()) {
                    retryRequest(staleRequest, retryCount, token)
                } else {
                    null
                }
            }
        } catch (ex: Exception) {
            Log.e("${this::class.java}", "$ex")
        }
        return null
    }

    private fun retryRequest(
        staleRequest: Request?,
        retryCount: Int,
        authToken: String
    ): Request? {
        return staleRequest?.newBuilder()
            ?.header(
                NetworkConstants.AUTHORIZATION,
                authToken
            )
            ?.header(
                NetworkConstants.API_RETRY_COUNT,
                "$retryCount"
            )
            ?.build()
    }
}

