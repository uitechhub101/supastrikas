package com.uitechhub.supastrikaz.api

import retrofit2.Call
import retrofit2.Response


/**
 * Retrofit result wrapper implemented using a sealed class to
 * have an enum that can handle different results.
 */
sealed class Result<T> {

    /**
     * Generic Success data class for retrofit onResponse args
     */
    data class Success<T>(val call: Call<T>?, val response: Response<T>): Result<T>()

    /**
     * Generic Failure data class for retrofit onFailure args
     */
    data class Failure<T>(val call: Call<T>, val error: Throwable): Result<T>()
}