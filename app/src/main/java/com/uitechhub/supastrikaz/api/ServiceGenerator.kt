package com.uitechhub.supastrikaz.api

import retrofit2.Retrofit
import javax.inject.Inject

class ServiceGenerator @Inject constructor(
    private val retrofitBuilder: Retrofit.Builder
) {

    val api: API by lazy {
        API.create(this.retrofitBuilder, "https://fakebaseurl")
    }
}