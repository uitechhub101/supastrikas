package com.uitechhub.supastrikaz.dao.persistence

import kotlin.reflect.KClass

interface SharedPrefsManager {

    fun getString(key: String, defaultValue: String = ""): String

    fun getInt(key: String): Int

    fun getLong(key: String): Long

    fun getBoolean(key: String): Boolean

    fun <T : Any> get(key: String, kClass: KClass<T>): T?

    fun set(key: String, value: String)

    fun set(key: String, value: Int)

    fun set(key: String, value: Long)

    fun set(key: String, value: Boolean)

    fun <T> put(key: String, value: T)

}