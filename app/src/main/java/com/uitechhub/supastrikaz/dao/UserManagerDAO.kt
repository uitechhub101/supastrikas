package com.uitechhub.supastrikaz.dao


interface UserManagerDAO {

    fun saveUserState(email: String, user: Any)

    fun getUserState(email: String): Any

}