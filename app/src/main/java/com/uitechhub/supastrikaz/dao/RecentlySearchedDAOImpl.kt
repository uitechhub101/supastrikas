package com.uitechhub.supastrikaz.dao

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager
import com.uitechhub.supastrikaz.extfuncs.move
import javax.inject.Inject

class RecentlySearchedDAOImpl @Inject constructor(
    var sharedPrefsManager: SharedPrefsManager,
    var gson: Gson
) : RecentlySearchedDAO {
    private val RecentlySearchedKey = "recently_searched"

    override fun saveRecentSearch(recentSearch: Any) {
        var recentSearches = getRecentlySearched().toMutableList()

        if (recentSearches.contains(recentSearch)) {
            recentSearches.move(recentSearch, 0)
        } else {
            recentSearches.add(0, recentSearch)
        }

        // We are only concerned with last 10 recent searches if list exceeds 10 truncate it
        if (recentSearches.size > 10) {
            sharedPrefsManager.set(RecentlySearchedKey, gson.toJson(recentSearches.subList(0, 10)))
        } else {
            sharedPrefsManager.set(RecentlySearchedKey, gson.toJson(recentSearches))
        }
    }

    override fun getRecentlySearched(): List<Any> {
        val recentSearchesJSON = sharedPrefsManager.getString(RecentlySearchedKey, "[]")
        val itemType = object : TypeToken<List<Any>>() {}.type
        return gson.fromJson(recentSearchesJSON, itemType)
    }

    override fun clearRecentlySearched(): List<Any> {
        sharedPrefsManager.set(RecentlySearchedKey, "[]")
        return listOf()
    }
}