package com.uitechhub.supastrikaz.dao.persistence

import android.content.SharedPreferences
import com.google.gson.Gson
import com.uitechhub.supastrikaz.extfuncs.get
import com.uitechhub.supastrikaz.extfuncs.put
import javax.inject.Inject
import kotlin.reflect.KClass

class EncryptedSharedPrefsManagerImpl @Inject constructor(
    var encryptedPreferences: SharedPreferences,
    val gson: Gson
) : SharedPrefsManager {

    override fun getString(key: String, defaultValue: String): String =
        encryptedPreferences.getString(key, defaultValue) ?: defaultValue

    override fun getInt(key: String): Int =
        encryptedPreferences.getInt(key, 0)

    override fun getLong(key: String): Long =
        encryptedPreferences.getLong(key, 0)

    override fun getBoolean(key: String): Boolean =
        encryptedPreferences.getBoolean(key, false)

    override fun <T : Any> get(key: String, kClass: KClass<T>): T? =
        encryptedPreferences.get(gson, key, kClass)

    override fun set(key: String, value: String) {
        getEditor().putString(key, value).commit()
    }

    override fun set(key: String, value: Int) {
        getEditor().putInt(key, value).commit()
    }

    override fun set(key: String, value: Long) {
        getEditor().putLong(key, value).commit()
    }

    override fun set(key: String, value: Boolean) {
        getEditor().putBoolean(key, value).commit()
    }

    override fun <T> put(key: String, value: T) {
        encryptedPreferences.put(gson, key, value)
    }

    private fun getEditor(): SharedPreferences.Editor {
        return encryptedPreferences.edit()
    }

    companion object {
        const val FIRSTNAME = "FIRSTNAME"
        const val ID = "ID"
        const val TOKEN = "TOKEN"
        const val EMAIL = "EMAIL"
        const val PASSWORD = "PASSWORD"
        const val LOGIN = "LOGIN_"
        const val LOCATION = "LOCATION"
        const val NOTIFICATIONS = "NOTIFICATIONS"
        const val CARTITEMS = "CARTITEMS"
    }
}