package com.uitechhub.supastrikaz.dao


interface RecentlyViewedDAO {

    fun saveProduct(product: Any)

    fun getRecentlyViewedProducts(): List<Any>
}