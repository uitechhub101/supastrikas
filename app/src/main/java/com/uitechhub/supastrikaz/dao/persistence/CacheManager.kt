package com.uitechhub.supastrikaz.dao.persistence

import okhttp3.Cache

interface CacheManager {

    fun getNetworkCache(cacheSize: Long): Cache

}