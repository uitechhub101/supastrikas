package com.uitechhub.supastrikaz.dao

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager
import javax.inject.Inject

class RecentlyViewedDAOImpl @Inject constructor(
    var sharedPrefsManager: SharedPrefsManager,
    var gson: Gson
): RecentlyViewedDAO {
    private val ViewedProductsKey = "recently_viewed_products"
    override fun saveProduct(product: Any) {
        try {
            val products = getRecentlyViewedProducts().toMutableList()
            products.add(0, product)
//            val requestProducts = if (products.size > 15) {
//                products.distinctBy { it.id }.subList(0, 15)
//            } else {
//                products.distinctBy { it.id }
//            }
            sharedPrefsManager.set(ViewedProductsKey, gson.toJson(products))
        } catch (e: Exception) {
            Log.e(this.javaClass.simpleName, "couldn't save recently viewed product", e)
        }
    }

    override fun getRecentlyViewedProducts(): List<Any> {
        val productsJSON = sharedPrefsManager.getString(ViewedProductsKey, "[]")
        val itemType = object : TypeToken<List<Any>>() {}.type
        var baseProducts =  gson.fromJson<List<Any>>(productsJSON, itemType)
        // Setting default style for recently viewed tiles, this should ensure consistency across app
        return baseProducts
    }
}