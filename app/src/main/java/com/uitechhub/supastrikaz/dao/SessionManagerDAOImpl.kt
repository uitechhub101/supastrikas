package com.uitechhub.supastrikaz.dao

import androidx.lifecycle.MutableLiveData
import com.uitechhub.supastrikaz.dao.persistence.EncryptedSharedPrefsManagerImpl
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager
import nl.komponents.kovenant.Promise
import javax.inject.Inject

class SessionManagerDAOImpl @Inject constructor(
    var sharedPrefsManager: SharedPrefsManager
) : SessionManagerDAO {

    override val connectivityLiveData: MutableLiveData<Boolean> = MutableLiveData()

    override fun isLoggedIn(): Boolean {
        return getToken().isNotEmpty()
    }

    override fun saveToken(token: String) {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.TOKEN, token)
    }

    override fun getToken(): String {
        return sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.TOKEN)
    }

    override fun saveEmail(email: String) {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.EMAIL, email)
    }

    override fun getEmail(): String {
        return sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.EMAIL)
    }

    override fun saveFirstName(name: String) {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.FIRSTNAME, name)
    }

    override fun getFirstName(): String {
        return sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.FIRSTNAME)
    }

    override fun saveID(id: String) {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.ID, id)
    }

    override fun getID(): String {
        return sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.ID)
    }

    override fun savePassword(password: String) {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.PASSWORD, password)
    }

    override fun getPassword(): String {
        return sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.PASSWORD)
    }

    override fun endSession() {
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.TOKEN, "")
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.EMAIL, "")
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.PASSWORD, "")
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.ID, "")
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.FIRSTNAME, "")
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.CARTITEMS, "")
    }

    override var refreshBearerToken: (() -> Promise<Any, Exception>)? = null

    override fun getRefreshToken() {
        refreshBearerToken?.invoke()?.get()
    }

    override fun getHasRequestedLocation(): Boolean =
        sharedPrefsManager.getInt(EncryptedSharedPrefsManagerImpl.LOCATION) == 1

    override fun setHasRequestedLocation() =
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.LOCATION, 1)

    override fun getPushCustomerId(): String =
        sharedPrefsManager.getString(EncryptedSharedPrefsManagerImpl.NOTIFICATIONS, "")

    override fun setPushCustomerId(pushCustomerId: String) =
        sharedPrefsManager.set(EncryptedSharedPrefsManagerImpl.NOTIFICATIONS, pushCustomerId)

}