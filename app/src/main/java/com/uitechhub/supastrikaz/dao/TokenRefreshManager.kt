package com.uitechhub.supastrikaz.dao

import nl.komponents.kovenant.Promise

/**
 * Responsible for invoking a token refresh using the assigned refreshBearerToken
 */
interface TokenRefreshManager {
    /**
     * Refresh bearer token hof.
     */
    var  refreshBearerToken: (() -> Promise<Any, Exception>)?

    /**
     * Call this to trigger an invoke function call on mutable refreshBearerToken hof.
     */
    fun getRefreshToken()

}