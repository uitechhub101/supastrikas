package com.uitechhub.supastrikaz.dao.persistence

import android.content.Context
import com.uitechhub.supastrikaz.dao.persistence.CacheManager
import okhttp3.Cache
import javax.inject.Inject


class CacheManagerImpl @Inject constructor(
    val context: Context
): CacheManager {

    override fun getNetworkCache(cacheSize: Long): Cache = Cache(context.cacheDir, cacheSize)


}