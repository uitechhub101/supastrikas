package com.uitechhub.supastrikaz.dao

import com.uitechhub.supastrikaz.dao.persistence.EncryptedSharedPrefsManagerImpl
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager
import javax.inject.Inject

class UserManagerDAOImpl @Inject constructor(
    var sharedPrefsManager: SharedPrefsManager
) : UserManagerDAO {

    override fun saveUserState(email: String, user: Any) {
        sharedPrefsManager.put(EncryptedSharedPrefsManagerImpl.LOGIN + email, user)
    }

    override fun getUserState(email: String): Any {
        return sharedPrefsManager.get(
            EncryptedSharedPrefsManagerImpl.LOGIN + email, Any::class
        ) ?: Any()
    }

}