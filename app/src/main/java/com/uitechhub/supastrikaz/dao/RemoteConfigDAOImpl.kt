package com.uitechhub.supastrikaz.dao

import com.uitechhub.supastrikaz.dao.persistence.CacheManager
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager


class RemoteConfigDAOImpl(
    val sharedPrefsManager: SharedPrefsManager,
    val cacheManager: CacheManager
): BaseDAO(),
    RemoteConfigDAO {

}