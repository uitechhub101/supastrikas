package com.uitechhub.supastrikaz.dao

import androidx.lifecycle.MutableLiveData

interface SessionManagerDAO : TokenRefreshManager {

    val connectivityLiveData: MutableLiveData<Boolean>

    fun isLoggedIn(): Boolean

    fun saveToken(token: String)

    fun getToken(): String

    fun saveEmail(email: String)

    fun getEmail(): String

    fun saveFirstName(name: String)

    fun getFirstName(): String

    fun saveID(id: String)

    fun getID(): String

    fun savePassword(password: String)

    fun getPassword(): String

    fun endSession()

    fun getHasRequestedLocation(): Boolean

    fun setHasRequestedLocation()

    fun getPushCustomerId(): String

    fun setPushCustomerId(pushCustomerId: String)
}