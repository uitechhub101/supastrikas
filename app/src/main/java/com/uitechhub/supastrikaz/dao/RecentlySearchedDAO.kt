package com.uitechhub.supastrikaz.dao


interface RecentlySearchedDAO {

    fun saveRecentSearch(recentSearch: Any)

    fun getRecentlySearched(): List<Any>

    fun clearRecentlySearched(): List<Any>
}