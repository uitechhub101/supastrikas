package com.uitechhub.supastrikaz.di.modules

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import com.uitechhub.supastrikaz.di.components.ActivityComponent
import dagger.Module
import dagger.Provides

@Module(subcomponents = [
    ActivityComponent::class
])
class ActivityModule(private val activity: FragmentActivity) {

    @Provides
    fun provideActivity(): AppCompatActivity = activity as AppCompatActivity

    @Provides
    fun provideFragmentManager(): FragmentManager = activity.supportFragmentManager

    @Provides
    fun provideViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory = factory
}