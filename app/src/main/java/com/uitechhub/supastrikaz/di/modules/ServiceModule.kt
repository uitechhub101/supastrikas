package com.uitechhub.supastrikaz.di.modules

import android.content.Context
import com.google.gson.Gson
import com.uitechhub.supastrikaz.api.API
import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.services.*
import com.uitechhub.supastrikaz.dao.SessionManagerDAO
import com.uitechhub.supastrikaz.di.annotation.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
open class ServiceModule {

    @Provides
    @ApplicationScope
    open fun provideRemoteConfigProvider(
        gson: Gson, api: API
    ): ConfigService = ConfigServiceImpl(gson, api)

    @Provides
    @ApplicationScope
    fun provideAuthenticationProvider(
        requestBuilder: RequestBuilder,
        strikazAPI: StrikazAPI,
        sessionManagerDAO: SessionManagerDAO
    ): AuthenticationService =
        AuthenticationServiceImpl(strikazAPI, requestBuilder, sessionManagerDAO)


    @Provides
    @ApplicationScope
    fun provideGPSLocationProvider(
        context: Context
    ): GPSLocationService = GPSLocationServiceImpl(context)

    @Provides
    @ApplicationScope
    fun provideLeagueService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): LeagueService = LeagueServiceImpl(strikazAPI, requestBuilder)


    @Provides
    @ApplicationScope
    fun provideSeasonService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): SeasonService = SeasonServiceImpl(strikazAPI, requestBuilder)

    @Provides
    @ApplicationScope
    fun provideTeamService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): TeamService = TeamServiceImpl(strikazAPI, requestBuilder)

    @Provides
    @ApplicationScope
    fun provideLiveScoreService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): LiveScoresService = LiveScoresServiceImpl(strikazAPI, requestBuilder)

    @Provides
    @ApplicationScope
    fun provideMarketService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): MarketsService =  MarketsServiceImpl(strikazAPI, requestBuilder)

    @Provides
    @ApplicationScope
    fun providePlayerService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): PlayerService = PlayerServiceImpl(strikazAPI, requestBuilder)

    @Provides
    @ApplicationScope
    fun provideFixtureService(
        strikazAPI: StrikazAPI,
        requestBuilder: RequestBuilder
    ): FixtureService = FixtureServiceImpl(strikazAPI, requestBuilder)
}