package com.uitechhub.supastrikaz.di.components

import androidx.lifecycle.ViewModel
import dagger.MapKey
import java.lang.annotation.*
import java.lang.annotation.Retention
import java.lang.annotation.Target
import kotlin.reflect.KClass
@Suppress("DEPRECATED_JAVA_ANNOTATION")
@MapKey
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)