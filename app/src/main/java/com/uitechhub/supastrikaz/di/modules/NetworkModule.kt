package com.uitechhub.supastrikaz.di.modules

import GSON
import android.content.Context
import android.net.ConnectivityManager
import com.uitechhub.supastrikaz.api.NetworkConstants
import com.uitechhub.supastrikaz.api.common.DevTrustSSLCerts
import com.uitechhub.supastrikaz.api.isNetworkAvailable
import com.uitechhub.supastrikaz.dao.persistence.CacheManager
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.uitechhub.supastrikaz.BuildConfig
import com.uitechhub.supastrikaz.api.API
import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.ServiceGenerator
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.interceptors.AuthorizationAuthenticator
import com.uitechhub.supastrikaz.api.interceptors.RequestInterceptor
import com.uitechhub.supastrikaz.dao.SessionManagerDAO
import com.uitechhub.supastrikaz.di.annotation.ApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager

@Module
open class NetworkModule {

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        if (BuildConfig.DEBUG) {
            this.level = HttpLoggingInterceptor.Level.HEADERS
            this.level = HttpLoggingInterceptor.Level.BODY
            redactHeader("Authorization")
        }
    }

    @Provides
    @Singleton
    open fun provideRequestInterceptor(
        sessionManagerDAO: SessionManagerDAO,
        connectivityManager: ConnectivityManager
    ): RequestInterceptor = object : RequestInterceptor() {
        override fun internetAvailable(): Boolean = connectivityManager.isNetworkAvailable()
        override fun getBearerToken(): String = sessionManagerDAO.getToken()
        override fun internetConnectionUpdate(active: Boolean) =
            sessionManagerDAO.connectivityLiveData.postValue(active)
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        // todo - add other properties (e.g typeAdapter dateFormat, fieldNamingPolicy, serializeNulls, version,)
        return GSON.create(prettyPrint = BuildConfig.DEBUG)
    }

    @Provides
    @ApplicationScope
    open fun provideServiceGenerator(
        gson: Gson,
        context: Context,
        retrofitBuilder: Retrofit.Builder
    ): ServiceGenerator {
        return ServiceGenerator(retrofitBuilder)
    }

    @Provides
    @Singleton
    fun provideAPIService(
        serviceGenerator: ServiceGenerator
    ): API = serviceGenerator.api

    @Provides
    @Singleton
    open fun provideRequestBuilder(gson: Gson): RequestBuilder = RequestBuilder(gson)

    @Provides
    fun provideStrikazAPI(
        api: API,
        gson: Gson,
        requestBuilder: RequestBuilder,
        sessionManagerDAO: SessionManagerDAO
    ): StrikazAPI {
        val strikazAPI = StrikazAPI(api, gson) { token -> sessionManagerDAO.saveToken(token) }
        sessionManagerDAO.refreshBearerToken = null/*{
            StrikazAPI.load(
                requestBuilder.login(sessionManagerDAO.getEmail(), sessionManagerDAO.getPassword())
            )
        }*/
        return strikazAPI
    }

    @Provides
    @Singleton
    fun provideAuthorizationInterceptor(
        sessionManagerDAO: SessionManagerDAO,
        connectivityManager: ConnectivityManager
    ): AuthorizationAuthenticator =
        AuthorizationAuthenticator(
            { sessionManagerDAO.getToken() },
            { sessionManagerDAO.getRefreshToken() },
            { sessionManagerDAO.isLoggedIn() },
            { connectivityManager.isNetworkAvailable() }
        )

    @Provides
    @Singleton
    open fun provideOkHttpClient(
        cacheManager: CacheManager,
        requestInterceptor: RequestInterceptor,
        loggingInterceptor: HttpLoggingInterceptor,
        authorizationAuthenticator: AuthorizationAuthenticator
    ): OkHttpClient {

        val builder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
//            val trustContext = DevTrustSSLCerts.trust()
//            if (trustContext != null) {
//                val sslSocketFactory: SSLSocketFactory = trustContext.socketFactory
//                builder.sslSocketFactory(
//                    sslSocketFactory,
//                    DevTrustSSLCerts.trustAllCerts[0] as X509TrustManager
//                )
//            }
        }
        return builder
            .addInterceptor(loggingInterceptor)
            .addInterceptor(requestInterceptor)
            .authenticator(authorizationAuthenticator)
            .readTimeout(NetworkConstants.CLIENT_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(NetworkConstants.CLIENT_TIME_OUT, TimeUnit.SECONDS)
            .connectTimeout(NetworkConstants.CLIENT_TIME_OUT, TimeUnit.SECONDS)
            .cache(cacheManager.getNetworkCache(NetworkConstants.MAX_NETWORK_CACHE_SIZE))
            .build()
    }

    @Singleton
    @Provides
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)


    @Singleton
    @Provides
    fun provideCoroutineCallAdapterFactory(): CoroutineCallAdapterFactory =
        CoroutineCallAdapterFactory()

    @Provides
    fun provideRetrofitBuilder(
        okHttpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        coroutineCallAdapterFactory: CoroutineCallAdapterFactory
    ): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(coroutineCallAdapterFactory)
            .client(okHttpClient)
    }
}