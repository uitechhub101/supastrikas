package com.uitechhub.supastrikaz.di.components

import com.uitechhub.supastrikaz.di.modules.ViewModelsModule
import com.uitechhub.supastrikaz.di.annotation.FragmentScope
import dagger.Subcomponent

@FragmentScope
@Subcomponent(
    modules = [
        ViewModelsModule::class
    ]
)
interface FragmentComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): FragmentComponent
    }

    // todo - Skeleton fragment KClass and layout needs to be created for commented out fragments.
//region Authentication
//    fun inject(fragment: LoginFragment)
//    fun inject(fragment: WelcomeFragment)
//    fun inject(fragment: UserNameFragment)
//    fun inject(fragment: ResetPasswordFragment)
//    fun inject(fragment: ForgotPasswordFragment)
//    fun inject(fragment: CreatePasswordFragment)
//    fun inject(fragment: OnBoardingFragment)
//    fun inject(fragment: ShareLocationFragment)
//endregion Authentication
//region Home
//    fun inject(fragment: HomeFragment)
//endregion Home
//region Account/Profile
//    fun inject(fragment: AccountFragment)
//    fun inject(fragment: AccountSettingsFragment)
//    fun inject(fragment: EditMyInfoFragment)
//    fun inject(fragment: ContactPreferencesFragment)
//endregion Account/Profile
//region Shop
//    fun inject(fragment: CartFragment)
//    fun inject(fragment: CheckoutFragment)
//    fun inject(fragment: PostOrderFragment)
// endregion Shop
}