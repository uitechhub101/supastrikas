package com.uitechhub.supastrikaz.di.modules

import androidx.fragment.app.Fragment
import dagger.Module
import dagger.Provides

@Module
class FragmentModule(private val fragment: Fragment) {
    @Provides
    internal fun providesFragment(): Fragment = fragment
}