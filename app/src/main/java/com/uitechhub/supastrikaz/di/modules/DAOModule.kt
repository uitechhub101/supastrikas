package com.uitechhub.supastrikaz.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.uitechhub.supastrikaz.dao.*
import com.uitechhub.supastrikaz.dao.persistence.CacheManager
import com.uitechhub.supastrikaz.dao.persistence.CacheManagerImpl
import com.uitechhub.supastrikaz.dao.persistence.EncryptedSharedPrefsManagerImpl
import com.uitechhub.supastrikaz.dao.persistence.SharedPrefsManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class DAOModule {

    @Provides
    @Singleton
    fun provideCacheManager(context: Context): CacheManager = CacheManagerImpl(context)

    @Provides
    @Singleton
    open fun provideSharedPrefsManager(
        sharedPrefs: SharedPreferences,
        encryptedPreferences: SharedPreferences,
        gson: Gson
    ): SharedPrefsManager = EncryptedSharedPrefsManagerImpl(sharedPrefs, gson)

    @Provides
    @Singleton
    fun provideRemoteConfigDAO(
        sharedPrefsManager: SharedPrefsManager,
        cacheManager: CacheManager
    ): RemoteConfigDAO = RemoteConfigDAOImpl(sharedPrefsManager, cacheManager)

    @Provides
    @Singleton
    fun provideRecentlyViewedDAO(
        sharedPrefsManager: SharedPrefsManager,
        gson: Gson
    ): RecentlyViewedDAO = RecentlyViewedDAOImpl(sharedPrefsManager, gson)

    @Provides
    @Singleton
    fun provideSessionManager(
        sharedPrefsManager: SharedPrefsManager
    ): SessionManagerDAO = SessionManagerDAOImpl(sharedPrefsManager)

    @Provides
    @Singleton
    fun provideRecentlySearchedDAO(
        sharedPrefsManager: SharedPrefsManager,
        gson: Gson
    ): RecentlySearchedDAO = RecentlySearchedDAOImpl(sharedPrefsManager, gson)

    @Provides
    @Singleton
    fun provideUserManager(
        sharedPrefsManager: SharedPrefsManager
    ): UserManagerDAO = UserManagerDAOImpl(sharedPrefsManager)
}