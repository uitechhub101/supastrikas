package com.uitechhub.supastrikaz.di.annotation

import javax.inject.Scope

@Scope
@kotlin.annotation.Retention
annotation class ActivityScope