package com.uitechhub.supastrikaz.di.components

import com.uitechhub.supastrikaz.di.modules.ViewModelsModule
import com.uitechhub.supastrikaz.ui.MainActivity
import com.uitechhub.supastrikaz.di.annotation.ActivityScope
import com.uitechhub.supastrikaz.ui.StrikazTesterActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [
    ViewModelsModule::class
])
interface ActivityComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): ActivityComponent
    }

    // inject activities here
    fun inject(activity: MainActivity)
    fun inject(activity: StrikazTesterActivity)
}