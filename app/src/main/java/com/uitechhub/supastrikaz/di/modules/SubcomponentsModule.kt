package com.uitechhub.supastrikaz.di.modules

import com.uitechhub.supastrikaz.di.components.ActivityComponent
import dagger.Module

@Module(subcomponents = [ActivityComponent::class])
class SubcomponentsModule {}