package com.uitechhub.supastrikaz.di.modules

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.uitechhub.supastrikaz.di.annotation.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
open class ApplicationModule(private val app: Application) {

    @Provides
    @ApplicationScope
    open fun provideAppContext(): Context = app.applicationContext

    @Provides
    @ApplicationScope
    open fun providePackageManager(): PackageManager = app.packageManager

    @Provides
    @ApplicationScope
    open fun provideSharedPrefs(context: Context): SharedPreferences  {
        val masterKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)
        return EncryptedSharedPreferences.create(
            "StrikazSharedPrefs", // fileName
            masterKeyAlias, // masterKeyAlias
            context, // context
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV, // prefKeyEncryptionScheme
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM // prefvalueEncryptionScheme
        )
    }

    @Provides
    @ApplicationScope
    open fun provideConnectivityManager(context: Context): ConnectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

}