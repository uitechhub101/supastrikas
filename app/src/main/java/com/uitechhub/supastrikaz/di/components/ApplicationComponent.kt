package com.uitechhub.supastrikaz.di.components

import com.uitechhub.supastrikaz.di.annotation.ApplicationScope
import com.uitechhub.supastrikaz.di.modules.*
import dagger.Component
import javax.inject.Singleton


@Singleton
@ApplicationScope
@Component(
    modules = [
        ApplicationModule::class,
        DAOModule::class,
        NetworkModule::class,
        ServiceModule::class,
        SubcomponentsModule::class,
        ViewModelsModule::class
    ]
)
interface ApplicationComponent {

    //inject sub-components
    fun activityComponent(): ActivityComponent.Factory
    fun fragmentComponent(): FragmentComponent.Factory

    // add other relevant inject methods here
//    fun inject(viewModel: AuthViewModel)

}
