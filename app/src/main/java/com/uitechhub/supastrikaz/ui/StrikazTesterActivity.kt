package com.uitechhub.supastrikaz.ui

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.google.gson.Gson
import com.uitechhub.supastrikaz.R
import com.uitechhub.supastrikaz.SuperStrikasApp
import com.uitechhub.supastrikaz.api.RequestBuilder
import com.uitechhub.supastrikaz.api.StrikazAPI
import com.uitechhub.supastrikaz.api.StrikazRequest
import com.uitechhub.supastrikaz.api.services.*
import com.uitechhub.supastrikaz.databinding.ActivityApitesterBinding
import nl.komponents.kovenant.Promise
import javax.inject.Inject


class StrikazTesterActivity : AppCompatActivity() {

    @Inject
    lateinit var gson: Gson

    @Inject
    lateinit var api: StrikazAPI

    @Inject
    lateinit var requestBuilder: RequestBuilder

    // todo - maybe avoid injecting all these providers and use requestBuilder and api
    @Inject
    lateinit var authService: AuthenticationService

    @Inject
    lateinit var teamService: TeamService

    @Inject
    lateinit var leagueService: LeagueService

    @Inject
    lateinit var seasonService: SeasonService

    @Inject
    lateinit var liveScoreService: LiveScoresService

    @Inject
    lateinit var marketService: MarketsService

    @Inject
    lateinit var playerService: PlayerService

    @Inject
    lateinit var fixtureService: FixtureService

    private var apis = listOf<ApiCaller>()
    private lateinit var adapter: APITesterListAdapter
    private lateinit var binding: ActivityApitesterBinding

    private var loading = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (applicationContext as SuperStrikasApp).getAppComponent().activityComponent().create()
            .inject(this)
        binding  = ActivityApitesterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.apiTesterResponseLabel.text = "Response: (none)"
        apis = getAPICallers()
        adapter = APITesterListAdapter(this, apis)
        binding.apiTesterList.adapter = adapter
        binding.apiTesterList.layoutManager = LinearLayoutManager(this)
        binding.apiTesterList.setHasFixedSize(true)
        binding.apiTesterList.addItemDecoration(
            DividerItemDecoration(this, VERTICAL)
        )
    }

    private fun loadAPICall(position: Int, call: StrikazRequest<*>) {
        if (loading) {
            return showToast()
        }
        showLoadingMessage()
        loading = true
        adapter.setClickable(position, false)
        val request = api.load(call)
        bindResponseToPromise(position, request)
    }

    private fun showToast(msg: String = "API call in progress") {
        Toast.makeText(this, msg, Toast.LENGTH_LONG)
            .show()
    }

    private fun getAPICallers(): List<ApiCaller> {
        return listOf(
            ApiCaller(
                name = "Get Leagues",
                description = "Get list of leagues",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Leagues",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = leagueService.getLeagues(
                                    listOf(inputs[0])
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Get Leagues by id",
                description = "Get list of leagues",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Leagues by ID",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val includes = if (inputs[1].isNotEmpty()) {
                                    listOf(inputs[1])
                                } else {
                                    null
                                }
                                val promise = leagueService.getLeaguesById(
                                    inputs[0], includes ?: listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                         "leagueId", "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Get Season by id",
                description = "Get Seasons by id",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Seasons by ID",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val includes = if (inputs[1].isNotEmpty()) {
                                    listOf(inputs[1])
                                } else {
                                    null
                                }
                                val promise = seasonService.getSeasonById(
                                    inputs[0].toInt(), includes ?: listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                         "season id", "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Search League By Name",
                description = "Search League by name",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Search League by name",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val includes = if (inputs[1].isNotEmpty()) {
                                    listOf(inputs[1])
                                } else {
                                    null
                                }
                                val promise = leagueService.searchLeagueByName(
                                    inputs[0], includes ?: listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "leagueName", "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Get Team By ID",
                description = "Get Team by ID",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Team by ID",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val includes = if (inputs[1].isNotEmpty()) {
                                    listOf(inputs[1])
                                } else {
                                    null
                                }
                                val promise = teamService.getTeamById(
                                    inputs[0], includes ?: listOf(), listOf(), listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "teamId", "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Search Team By Name",
                description = "Search Team by name",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Search Team by name",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val includes = if (inputs[1].isNotEmpty()) {
                                    listOf(inputs[1])
                                } else {
                                    null
                                }
                                val promise = teamService.searchTeamByName(
                                    inputs[0], includes ?: listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "teamName", "includes"
                    )
                }
            ),
            ApiCaller(
                name = "Get All Livescores",
                description = "Get all live scores. Enrichment values are includes, leagues, markets, fixtures, bookmakers",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get All Live scores.",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = liveScoreService.getAllLiveScores(
                                    listOf(inputs[0]), listOf(), listOf(), listOf(), listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "includes", "leagues", "markets", "fixtures", "bookmakers"
                    )
                }
            ),
            ApiCaller(
                name = "Get Livescores now",
                description = "Get live scores now. Enrichment values are includes, leagues, markets, fixtures, bookmakers",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get All Live scores.",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = liveScoreService.getLiveScoresNow(
                                    listOf(inputs[0]), listOf(), listOf(), listOf(), listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        },
                        "includes", "leagues", "markets", "fixtures", "bookmakers"
                    )
                }
            ),
            ApiCaller(
                name = "Get All Markets",
                description = "Retrieves all available markets.",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get All Markets.",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = marketService.getAllMarkets()
                                bindResponseToPromise(position, promise)
                            }
                        }, ""
                    )
                }
            ),
            ApiCaller(
                name = "Get Markets by ID",
                description = "Retrieves market by specified ID.",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Markets by specified ID",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = marketService.getMarketsByID(
                                    inputs[0]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Market Id"
                    )
                }
            ),
            ApiCaller(
                name = "Get all fixtures by Markets ID",
                description = "Retrieves fixtures by specified market ID.",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get fixtures by specified Markets ID",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = marketService.getAllFixturesByMarketID(
                                    inputs[0]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Market Id"
                    )
                }
            ),
            ApiCaller(
                name = "Get Standings by seasonId",
                description = "Retrieves Standings by specified season ID",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Standings by seasonId",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = seasonService.getStandings(
                                    inputs[0], null, null, null
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Season Id"
                    )
                }
            ),
            ApiCaller(
                name = "Get Standings by seasonId and date",
                description = "Retrieves Standings by specified season ID and date (formatted - yyyy-mm-dd)",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Standings by seasonId and date",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = seasonService.getStandings(
                                    inputs[0], inputs[1]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Season Id", "Date (YYYY-MM-DD)"
                    )
                }
            ),
            ApiCaller(
                name = "Get Standings corrections by seasonId",
                description = "Retrieves corrections to Standings by specified season ID",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Standings corrections by seasonId",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = seasonService.getStandingsCorrections(
                                    inputs[0]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Season Id"
                    )
                }
            ),
            ApiCaller(
                name = "Get Player by Id",
                description = "Retrieves a PlayerResponse object that matches the Id supplied",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Player by Id",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = playerService.getPlayerById(
                                    inputs[0], listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Player Id"
                    )
                }
            ),
            ApiCaller(
                name = "Search Player by name",
                description = "Retrieves a PlayerResponse object that matches the playerName supplied",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Search Player by name",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = playerService.searchPlayerByName(
                                    inputs[0], playerService.playerIncludes
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Player Name"
                    )
                }
            ),
            ApiCaller(
                name = "Get Players For Country Id",
                description = "Retrieves List<PlayerResponse> for specified countryId",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Players For Country Id",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = playerService.getPlayersForCountryId(
                                    inputs[0], listOf()
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "CountryId"
                    )
                }
            ),
            ApiCaller(
                name = "Get Fixtures By Id",
                description = "Retrieves FixtureResponse for specified fixtureId",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Fixtures By Id",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = fixtureService.getFixturesById(
                                    inputs[0]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Fixture Id"
                    )
                }
            ),
            ApiCaller(
                name = "Get Last Updated Fixtures",
                description = "Retrieves all fixtures with updates ",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Last Updated Fixtures",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = fixtureService.getLastUpdatedFixtures()
                                bindResponseToPromise(position, promise)
                            }
                        },""
                    )
                }
            ),
            ApiCaller(
                name = "Get Fixtures By Date",
                description = "Retrieves all fixtures by date ",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Fixtures By Date",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = fixtureService.getFixturesByDate(
                                    inputs[0]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Date(yyyy-mm-dd)"
                    )
                }
            ),
            ApiCaller(
                name = "Get Fixtures By Date Range",
                description = "Retrieves all fixtures by date range",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Fixtures By Date Range",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = fixtureService.getFixturesByDateRange(
                                    inputs[0], inputs[1]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Start Date(yyyy-mm-dd)", "End Date(yyyy-mm-dd)"
                    )
                }
            ),
            ApiCaller(
                name = "Get Fixtures By Date Range For Team Id",
                description = "Retrieves all fixtures by date range for specified team",
                load = { position ->
                    showArgsRequestDialog(
                        position,
                        "Get Fixtures By Date Range for team id",
                        { inputs: List<String> ->
                            if (loading) {
                                showToast()
                            } else {
                                showLoadingMessage()
                                loading = true
                                adapter.setClickable(position, false)
                                val promise = fixtureService.getFixturesByDateRangeForTeam(
                                    inputs[0], inputs[1], inputs[2]
                                )
                                bindResponseToPromise(position, promise)
                            }
                        }, "Start Date(yyyy-mm-dd)", "End Date(yyyy-mm-dd)", "Team Id"
                    )
                }
            )


        )
    }


    private fun bindResponseToPromise(
        position: Int,
        promise: Promise<Any?, Exception>
    ) {
        promise.success { response ->
            showResponse(response, success = true)
        }.fail { e ->
            showResponse(e.message, success = false)
        }.always {
            reEnableListPosition(position)
        }
    }

    private fun reEnableListPosition(position: Int) {
        apis[position].enabled = true
        binding.apiTesterList.adapter?.notifyItemChanged(position)
        loading = false
        adapter.setClickable(position, true)
    }

    private fun showLoadingMessage() {
        binding.apiTesterResponseLabel.text = "Response: (loading)"
        binding.apiTesterResponseValue.text = ""
    }

    private fun showResponse(response: Any?, success: Boolean) {
        var responseStr = when (response) {
            is String -> response
            is String? -> ""
            else -> try {
                gson.toJson(response)
            } catch (e: Exception) {
                response.toString()
            }
        }

        // Trim output just in case we're showing raw json
        if (responseStr.length > 10000) {
            responseStr = responseStr.substring(0, 10000)
        }

        val responseLabel = if (success) "Success" else "Failure"

        val colorResInt =
            if (success) android.R.color.holo_green_dark else android.R.color.holo_red_dark

        runOnUiThread {
            binding.apiTesterResponseLabel.text = "Response: $responseLabel"
            binding.apiTesterResponseLabel.setTextColor(ContextCompat.getColor(this, colorResInt))

            binding.apiTesterResponseValue.text =
                if (responseStr.isEmpty()) "(empty response)" else responseStr
        }
    }

    /**
     * Show a dialog to accept runtime arguments for a given API
     *
     * @param argNames The names of the argument fields.
     */
    private fun showArgsRequestDialog(
        position: Int,
        apiName: String,
        loadApiCallback: (List<String>) -> Unit,
        vararg argNames: String
    ) {
        val pairs = argNames.map { Pair(it, "") }.toTypedArray()

        showArgsRequestDialog(position, apiName, loadApiCallback, *pairs)
    }

    /**
     * Show a dialog to accept runtime arguments for a given API.
     *
     * @param argNamesAndValues Name/Value pairs for each argument where the name is the argument
     *                          name and the value is the default value for that argument.
     */
    private fun showArgsRequestDialog(
        position: Int,
        apiName: String,
        loadApiCallback: (List<String>) -> Unit,
        vararg argNamesAndValues: Pair<String, String>
    ) {
        val dialog = RequestApiArgumentsDialog.newInstance(
            this,
            apiName,
            argNamesAndValues,
            object : IAlertDialogListener {
                override fun onPositiveButtonClicked(argumentValues: MutableList<String>) {
                    loadApiCallback(argumentValues.toList())
                }

                override fun onNegativeButtonClicked() {
                    // override method is not needed
                }
            }
        ) { reEnableListPosition(position) }

        dialog.show(supportFragmentManager, RequestApiArgumentsDialog::class.java.simpleName)
    }


    internal data class ApiCaller(
        val name: String,
        val description: String,
        val load: (position: Int) -> Unit,
        var enabled: Boolean = true
    )

    internal class APITesterListAdapter(
        private val context: Context,
        private val apis: List<ApiCaller>
    ) : RecyclerView.Adapter<APIListViewHolder>() {

        private val enabledPositions = BooleanArray(apis.size, { true })

        fun setClickable(position: Int, enabled: Boolean) {
            if (position in 0..enabledPositions.size) {
                enabledPositions[position] = enabled
            }
        }

        override fun onBindViewHolder(holder: APIListViewHolder, position: Int) {
            holder.bind(apis[position]) { enabledPositions[position] }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): APIListViewHolder {
            return APIListViewHolder(
                LayoutInflater
                    .from(context)
                    .inflate(R.layout.list_item_two_label, parent, false)
            )
        }

        override fun getItemCount() = apis.size
    }

    internal class APIListViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        private val title = view.findViewById<TextView>(R.id.list_item_two_label_title)
        private val subtitle = view.findViewById<TextView>(R.id.list_item_two_label_subtitle)

        private val white = ContextCompat.getColor(view.context, R.color.white)
        private val darkGray = ContextCompat.getColor(view.context, R.color.dark_gray)
        private val lightGray = ContextCompat.getColor(view.context, R.color.light_gray)

        private val titleColors = Pair(title.currentTextColor, darkGray)
        private val subtitleColors = Pair(subtitle.currentTextColor, darkGray)
        private val backgroundColors = Pair(white, lightGray)

        fun bind(apiCaller: ApiCaller, clicksEnabled: () -> Boolean) {
            title.text = apiCaller.name
            subtitle.text = apiCaller.description

            subtitle.visibility = if (subtitle.text.isEmpty()) View.GONE else View.VISIBLE

            updateTextColors(apiCaller.enabled)

            view.setOnClickListener {
                if (clicksEnabled()) {
                    apiCaller.enabled = false

                    updateTextColors(apiCaller.enabled)
                    apiCaller.load(adapterPosition)
                }
            }
        }

        private fun updateTextColors(enabled: Boolean) {
            view.setBackgroundColor(if (enabled) backgroundColors.first else backgroundColors.second)
            title.setTextColor(if (enabled) titleColors.first else titleColors.second)
            subtitle.setTextColor(if (enabled) subtitleColors.first else subtitleColors.second)
        }
    }

    internal class RequestApiArgumentsDialog(val extContext: Context) : DialogFragment(),
        View.OnClickListener {

        lateinit var argNamesAndValues: List<Pair<String, String>>
        var listener: IAlertDialogListener? = null
        var apiCallName: String = ""
        var dismissListener: () -> Unit = {}
        private var recyclerView: RecyclerView? = null

        companion object {
            fun newInstance(
                context: Context,
                apiName: String,
                argNameList: Array<out Pair<String, String>>,
                callback: IAlertDialogListener,
                dismissListener: () -> Unit
            ): RequestApiArgumentsDialog {
                val fragment = RequestApiArgumentsDialog(context)
                fragment.argNamesAndValues = argNameList.toList()
                fragment.apiCallName = apiName
                fragment.listener = callback
                fragment.dismissListener = dismissListener


                return fragment
            }
        }

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val dialog = Dialog(extContext)

            val rootView = LayoutInflater.from(extContext)
                .inflate(R.layout.fragment_request_api_arguments_dialog, null)

            val dialogTitle = rootView.findViewById<TextView>(R.id.alertDialogTitle)
            dialogTitle.text =
                context?.getString(R.string.request_api_arg_dialog_title, apiCallName)

            val cancelBtn = rootView.findViewById<Button>(R.id.alertDialogNegativeBtn)
            cancelBtn.setOnClickListener(this)

            val submitBtn = rootView.findViewById<Button>(R.id.alertDialogPositiveBtn)
            submitBtn.setOnClickListener(this)

            recyclerView = rootView.findViewById(R.id.argumentList)
            recyclerView?.layoutManager = LinearLayoutManager(context)
            recyclerView?.adapter = RequestApiArgumentsAdapter(argNamesAndValues)

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(rootView)

            // Require the user to press cancel instead of tapping outside or pressing back
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            this.isCancelable = false

            dialog.setOnDismissListener { dismissListener() }

            return dialog
        }

        override fun onClick(view: View?) {
            val viewID = view?.id

            val argValues = mutableListOf<String>()
            if (viewID == R.id.alertDialogPositiveBtn) {
                val adapter = recyclerView?.adapter as RequestApiArgumentsAdapter
                (0..adapter.itemCount.minus(1))
                    .map {
                        val holder =
                            recyclerView?.findViewHolderForAdapterPosition(it) as RequestApiArgumentsViewHolder
                        argValues.add(holder.view.text.toString().trim())
                    }

                listener?.onPositiveButtonClicked(argValues)
            }

            if (viewID == R.id.alertDialogNegativeBtn) {
                listener?.onNegativeButtonClicked()
                dismissListener()
            }

            dismiss()
        }

        override fun show(manager: FragmentManager, tag: String?) {
            val fragmentTransaction = manager.beginTransaction()

            fragmentTransaction.add(this, tag)
            fragmentTransaction.commitAllowingStateLoss()
        }

        internal class RequestApiArgumentsViewHolder(val view: EditText) :
            RecyclerView.ViewHolder(view)

        internal class RequestApiArgumentsAdapter(private val argNamesAndValues: List<Pair<String, String>>) :
            RecyclerView.Adapter<RequestApiArgumentsViewHolder>() {

            override fun getItemCount(): Int = argNamesAndValues.size

            override fun onBindViewHolder(holder: RequestApiArgumentsViewHolder, position: Int) {
                // todo - this could be extended to use different view holders.
                holder.view.hint = argNamesAndValues[position].first
                holder.view.setText(
                    argNamesAndValues[position].second,
                    TextView.BufferType.EDITABLE
                )
            }

            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): RequestApiArgumentsViewHolder {
                val view = EditText(parent.context)
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                view.layoutParams = params

                return RequestApiArgumentsViewHolder(view)
            }
        }
    }
}



internal interface IAlertDialogListener {
    fun onPositiveButtonClicked(argumentValues: MutableList<String>)
    fun onNegativeButtonClicked()
}