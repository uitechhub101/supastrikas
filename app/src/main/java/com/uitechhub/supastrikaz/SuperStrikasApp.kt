package com.uitechhub.supastrikaz

import android.app.Application
import com.uitechhub.supastrikaz.di.components.ApplicationComponent
import com.uitechhub.supastrikaz.di.components.DaggerApplicationComponent
import com.uitechhub.supastrikaz.di.modules.ApplicationModule
import com.uitechhub.supastrikaz.di.modules.DAOModule
import com.uitechhub.supastrikaz.di.modules.NetworkModule
import com.uitechhub.supastrikaz.di.modules.ServiceModule

class SuperStrikasApp : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    private fun initialize() {
        injectAppComponent()
        //todo - add other app initialization code here.
    }

    override fun onCreate() {
        super.onCreate()
        initialize()
    }

    private fun injectAppComponent() {
        if (!::applicationComponent.isInitialized) {
            applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .networkModule(NetworkModule())
                .serviceModule(ServiceModule())
                .dAOModule(DAOModule())
                .build()
        }
    }

    fun getAppComponent() = applicationComponent

}
