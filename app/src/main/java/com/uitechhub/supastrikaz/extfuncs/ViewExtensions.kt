package com.uitechhub.supastrikaz.extfuncs

import android.view.View
import android.view.ViewGroup

fun View.toggleVisibility(visible: Boolean) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun ViewGroup.toggleVisibility(visible: Boolean) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}