package com.uitechhub.supastrikaz.extfuncs

import android.content.SharedPreferences
import com.google.gson.Gson
import kotlin.reflect.KClass

fun <T : Any> SharedPreferences.get(gson: Gson, key: String, c: KClass<T>): T? {
    val value = getString(key, null)
    return gson.fromJson(value, c.java)
}

fun <T> SharedPreferences.put(gson: Gson, key: String, `object`: T) {
    val jsonString = gson.toJson(`object`)
    edit().putString(key, jsonString).commit()
}