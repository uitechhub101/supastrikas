package com.uitechhub.supastrikaz.extfuncs

import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Patterns
import android.view.View
import android.widget.TextView
import java.text.SimpleDateFormat

/**
 * Calls [String.replace] for each key/value pair in the given map.
 */
fun String.replaceAll(map: Map<String, String>): String {
    var result = this

    map.entries.forEach { (key, value) -> result = result.replace(key, value) }

    return result
}

fun String.appendQueryParameters(map: Map<String, String>): String {
    var result = this
    if (map.isNotEmpty()) {
        var appendString = "?"
        map.entries.forEach { (key, value) ->
            appendString += "$key=$value&"
        }

        result += appendString.removeRange(appendString.lastIndexOf("&"), appendString.length)
    }

    return result
}


/**
 *  Call addLinks when you want parts of a string to be clickable
 *  @param textView view on screen that needs links.
 *  @param spanTextColor color applied to text span.
 *  @param underlineText boolean indicating if text span should be underlined
 *  @param spans a list of Pair objects of type Pair<String, () -> Unit>
 */
fun String.addLinks(
    textView: TextView,
    spanTextColor: Int,
    underlineText: Boolean = false,
    spans: List<Pair<String, () -> Unit?>>
) {
    if (spans.isNotEmpty()) {
        val text = SpannableString(this)
        for (span in spans) {
            val clickableSpan = object : ClickableSpan() {
                override fun onClick(widget: View) {
                    span.second.invoke()
                }

                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = underlineText
                    ds.isFakeBoldText = true
                    ds.color = spanTextColor
                }
            }
            val termsIndex = text.indexOf(span.first, 0, true)
            text.setSpan(
                clickableSpan, termsIndex, termsIndex + span.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        textView.text = text
        textView.movementMethod = LinkMovementMethod.getInstance()
    }
}

fun String.isValidEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.toTitleCase(): String {
    return this.split(" ").joinToString(" ") { it.capitalize() }
}

fun String.formatOrderDate(addCommaAfterDay: Boolean = false): String {
    val outPutPattern = if (addCommaAfterDay) "LLL d, yyyy" else "LLL d yyyy"
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm")
    val outputFormat = SimpleDateFormat(outPutPattern)
    return outputFormat.format(inputFormat.parse(this)!!)
}


fun String.parseCmsUrlCategoryId(): String = Uri.parse(this).path.toString().replace("/", "")

fun String.parseCmsUrlFilterId(): String = Uri.parse(this).getQueryParameter("prefn1").toString()

fun String.parseCmsUrlFilterValue(): String = Uri.parse(this).getQueryParameter("prefv1").toString()

fun String.parseCmsUrlQueryValue(): String = Uri.parse(this).query.toString().replace("q=", "")
