plugins {
    id 'com.android.application'
    id 'kotlin-android'
}
apply plugin: 'com.google.gms.google-services'
apply plugin: 'com.google.firebase.crashlytics'
apply plugin: 'com.google.firebase.firebase-perf'
apply plugin: 'kotlin-kapt'
apply plugin: "androidx.navigation.safeargs.kotlin"
apply from: './secrets.gradle'

def getAppName() {
    def stringsFile = android.sourceSets.main.res.sourceFiles.find { it.name.equals 'strings.xml' }
    return new XmlParser().parse(stringsFile).string.find {
        it.@name.equals 'app_name_static'
    }.text()
}

android {
    compileSdkVersion 31
    buildToolsVersion "31.0.0"

    defaultConfig {
        applicationId "com.uitechhub.supastrikas"
        minSdkVersion 23
        targetSdkVersion 31
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        debug {
            minifyEnabled false
            debuggable true
            testCoverageEnabled true
        }
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
    flavorDimensions "environment"
    productFlavors {
        production {
            dimension "environment"
            buildConfigField "String", "MONKS_API_TOKEN", "\"$MONKS_API_PROD_KEY\""
//            buildConfigField "String", "FIREBASE_SENDER_ID", "\"${FIREBASE_SENDER_ID}\""
        }
        development {
            getIsDefault().set(true) // load IDE into development Debug by default
            applicationId 'com.uitechhub.dev.supastrikas'
            dimension "environment"
            versionNameSuffix "-dev"
            buildConfigField "String", "MONKS_API_TOKEN", "\"$MONKS_API_DEV_KEY\""
//            buildConfigField "String", "FIREBASE_SENDER_ID", "\"${FIREBASE_SENDER_ID}\""
        }
    }
    applicationVariants.all { variant ->
        if (variant.productFlavors.get(0).name == "development") {
            variant.resValue "string", "app_name", "SS-DEV"
        } else {
            variant.resValue "string", "app_name", getAppName()
        }
    }
    compileOptions {
        sourceCompatibility JavaVersion.VERSION_11
        targetCompatibility JavaVersion.VERSION_11
    }
    kotlinOptions {
        jvmTarget = '1.8'
    }
    buildFeatures {
        viewBinding true
    }
}

dependencies {

    implementation appDeps.android.x.appCompat
    implementation appDeps.android.x.supportv4
    implementation appDeps.android.x.coreKtx
    implementation appDeps.android.x.recyclerView
    implementation appDeps.android.x.viewPager2
    implementation appDeps.android.x.constraintLayout
    implementation appDeps.android.x.lifecycle.runtime
    implementation appDeps.android.x.lifecycle.liveData
    implementation appDeps.android.x.lifecycle.viewModel
//    implementation appDeps.android.x.lifecycle.extensions
    implementation appDeps.android.x.security.crypto
    implementation appDeps.android.x.navigation.ui
    implementation appDeps.android.x.navigation.fragment


    // arch components
    kapt appDeps.android.arch.compiler
    implementation appDeps.android.arch.viewModel
    implementation appDeps.android.arch.extensions

    implementation appDeps.google.gson
    implementation appDeps.google.design
    implementation appDeps.google.firebase.fcm
    implementation appDeps.google.firebase.perf
    implementation appDeps.google.firebase.analytics
    implementation appDeps.google.firebase.crashlytics

    // Kotlin  & kotlin Coroutines
    implementation appDeps.kotlin.stdLib
    implementation appDeps.kotlin.reflect
    implementation appDeps.kotlin.coroutinesCore
    implementation appDeps.kotlin.coroutinesAndroid

    // Network
    implementation appDeps.squareUp.okio
    implementation appDeps.squareUp.okhttp
    implementation appDeps.squareUp.retrofit
    implementation appDeps.squareUp.okhttpLogInterceptor
    implementation appDeps.squareUp.retrofitGsonConverter
    implementation appDeps.squareUp.retrofitCoroutinesAdapter
    implementation appDeps.kovenant

    // di
    implementation appDeps.google.dagger.di
    kapt appDeps.google.dagger.compiler
    kaptTest appDeps.google.dagger.compiler

    //lottie animations
    implementation appDeps.lottie
    // location services
    implementation appDeps.google.locationPlayServices

    // runtime permissions
    implementation appDeps.easyPermissions
    // additive animations for Android
    implementation appDeps.easing
    implementation appDeps.yoyo

    // glide - image loading framework
    implementation appDeps.glide.lib
    kapt appDeps.glide.compiler

    // shimmer
    implementation appDeps.shimmer

    // photoview
    implementation appDeps.photoview
    testImplementation 'junit:junit:4.+'
    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'
}