

RELEASE_NOTES=$(
  curl  --compressed --request GET \
  --url 'add url to android issues in done column in Jira' \
  --header 'Accept: */*' \
  --header 'Accept-Encoding: gzip, deflate' \
  --header 'Authorization: Basic c2hhd24udGVzdGVyQHRpbnJvb2Zzb2Z0d2FyZS5jb206SVF1WHM0ZjFFZjlJeVdBZnQweWg1REUw' \
  --header 'Cache-Control: no-cache' \
  --header 'Connection: keep-alive' \
  --header 'Cookie: atlassian.xsrf.token=BCBU-JCOT-6SSZ-X71R_5c46aea33271536be52cdd42c0af7c13b972acf8_lin' \
  --header 'Host:Add host name here.' \
  --header 'Postman-Token: 8ca9d4c5-0829-4707-ae64-3d8de91e498a,7f7fb006-25b6-4349-b1c6-d85333bf54dc' \
  --header 'User-Agent: PostmanRuntime/7.16.3' \
  --header 'cache-control: no-cache' | jq -r '.issues[] | "https://add jira link here/browse/\(.key)", .fields.summary, "----------------------------------------------------------"'
)

if [ "$RELEASE_NOTES" == "" ]; then
    echo "No release notes."
    RELEASE_NOTES="No Release notes. Please consult Jira board: https://add  jira link/secure/RapidBoard.jspa?rapidView=193"
fi

echo $RELEASE_NOTES

RELEASE_FILE="app/build/outputs/apk/development/debug/app-development-debug.apk"
RELEASE_GROUP="Release Group name"
RELEASE_APP="Release app name"

if [ "${CIRCLE_BRANCH}" == "uat" ]
then
  RELEASE_FILE="app/build/outputs/apk/production/debug/app-production-debug.apk"
  RELEASE_GROUP="Release group name"
  RELEASE_APP="Release app name"
  RELEASE_NOTES="Release notes here"
fi