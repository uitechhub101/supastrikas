#!/bin/bash

PROPS_FILE="gradle.properties"

echo "" >> $PROPS_FILE

if [ "$CIRCLE_PULL_REQUEST" == "" ]; then
  echo "Not a Pull Request"

  echo "systemProp.sonar.branch.name=$CIRCLE_BRANCH" >> $PROPS_FILE

  if [ "$CIRCLE_BRANCH" != "develop" ]; then
    echo "systemProp.sonar.branch.target=develop" >> $PROPS_FILE
  fi
else
  echo "Pull Request '$CIRCLE_PULL_REQUEST'"
  PR_KEY=$(sed "s/.*\///" <<< "$CIRCLE_PULL_REQUEST")
  echo "PR_KEY=$PR_KEY"

  echo "systemProp.sonar.pullrequest.branch=$CIRCLE_BRANCH" >> $PROPS_FILE
  echo "systemProp.sonar.pullrequest.key=$PR_KEY" >> $PROPS_FILE

  if [ "$CIRCLE_BRANCH" == "develop" ]; then
    echo "systemProp.sonar.pullrequest.base=master" >> $PROPS_FILE
  else
    echo "systemProp.sonar.pullrequest.base=develop" >> $PROPS_FILE
  fi
fi

echo "systemProp.sonar.login=$SONAR_TOKEN" >> $PROPS_FILE

cat $PROPS_FILE