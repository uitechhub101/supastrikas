if [ "${CIRCLE_BRANCH}" == "uat" ]
then
    ./gradlew :app:assembleProductionDebug
else
    ./gradlew :app:assembleDevelopmentDebug -PincrementBuildNumber
fi