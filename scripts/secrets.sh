#!/bin/bash

KEY_FILE="app/secrets.gradle"

rm -f "$KEY_FILE"
touch "$KEY_FILE"

echo "ext {" >> "$KEY_FILE"
if [ "MONKS_API_DEV_KEY" != "" ]; then
  echo "  MONKS_API_DEV_KEY=\"${MONKS_API_DEV_KEY}\"" >> "$KEY_FILE"
fi
if [ "MONKS_API_PROD_KEY" != "" ]; then
  echo "  MONKS_API_PROD_KEY=\"${MONKS_API_PROD_KEY}\"" >> "$KEY_FILE"
fi
echo "}" >> "$KEY_FILE"


